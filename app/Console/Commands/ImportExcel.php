<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Excel;
use Carbon\Carbon;
use App\Models\Company;
use App\Models\CompanyName;
use App\Models\ContactPerson;
use App\Models\Event;

class ImportExcel extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'import';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import excel file';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$file_name = 'all.xlsx';
		// $file_name = 'lite.xlsx';

		$all_data = Excel::load('storage/app/excel/' . $file_name)->get();
		$paper = $all_data[0];

		foreach ($paper as $row) {
			if ($row->menedzher) {

				switch ($row->menedzher) {
					case 'Пахомов Д.':
						$parameters['manager_id'] = 4;
						break;
					case '           Рыжкова Е.':
						$parameters['manager_id'] = 5;
						break;
					case 'Рыжкова Е.':
						$parameters['manager_id'] = 5;
						break;
					case 'Тимофеев Н.':
						$parameters['manager_id'] = 6;
						break;
					default:
						die('Строка с несуществующим менеджером: ' . $row->menedzher);
						break;
				}

				switch ($row->vkhod_iskhod) {
					case 'исх':
						$parameters['type'] = 'out';
						$parameters['request_number'] = null;
						break;
					case 'исход':
						$parameters['type'] = 'out';
						$parameters['request_number'] = null;
						break;
					case 'вход':
						$parameters['type'] = 'in';
						$parameters['request_number'] = $row->vkhod;
						break;
					case 'вх':
						$parameters['type'] = 'in';
						$parameters['request_number'] = $row->vkhod;
						break;
					default:
						die('Проблема: ' . $row);
						break;
				}

				$parameters['tonnazh'] = $row->tonnazh;
				$parameters['kp'] = $row->kp;
				$parameters['company_name'] = $row->nazvanie_kompanii;
				$parameters['contact_person_name'] = $row->kontaktnoe_litso;
				$parameters['contact_person_phone'] = $row->tel;
				$parameters['contact_person_email'] = $row->pochta;
				$parameters['address'] = $row->adres === null? '' : $row->adres;
				$parameters['comment'] = $row->peregovory_kommentarii === null? '' : $row->peregovory_kommentarii;

				// $parameters['confirmed_at'] = $row->data_nachala_raboty === null? Carbon::now() : $row->data_nachala_raboty;
				if ($row->data_nachala_raboty === null || !is_a($row->data_nachala_raboty, 'Carbon\Carbon') || Carbon::createFromDate(2000, 1, 1)->gt($row->data_nachala_raboty) || Carbon::createFromDate(2016, 1, 1)->lt($row->data_nachala_raboty)) {
					$parameters['confirmed_at'] = Carbon::now();
				} else {
					$parameters['confirmed_at'] = $row->data_nachala_raboty;
				}

				self::importRow($parameters);
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// ['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			// ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

	private function importRow($parameters)
	{
		$input['created_user_id'] = 1;
		$input['updated_user_id'] = 1;
		$input['confirmed_at'] = $parameters['confirmed_at'];
		$input['current_manager_id'] = $parameters['manager_id'];
		$input['address'] = $parameters['address'];

		if ('in' === $parameters['type']) {
			$input['source_id'] = 1;
			$input['request_number'] = 0;
		} else {
			$input['source_id'] = 8;
			$input['request_number'] = null;
		}

		if ($parameters['request_number']) {
			$input['request_number'] = $parameters['request_number'];
		}

		// var_dump($input['request_number']);

		if (!empty(trim($parameters['company_name']))) {
			$company_names[] = new CompanyName(['title' => $parameters['company_name']]);
		}

		if (!empty(trim($parameters['contact_person_name'])) || !empty(trim($parameters['contact_person_phone'])) || !empty(trim($parameters['contact_person_email']))) {
			$contact_persons[] = new ContactPerson([
				'name' => (string) $parameters['contact_person_name'],
				'phone' => (string) $parameters['contact_person_phone'],
				'email' => (string) $parameters['contact_person_email'],
			]);
		}

		$company = Company::create($input);

		if (!empty($company_names)) {
			$company->names()->saveMany($company_names);
		}
		if (!empty($contact_persons)) {
			$company->contact_persons()->saveMany($contact_persons);
		}

		$company->managers()->create(['user_id' => $input['current_manager_id']]);

		// add event

		if ($parameters['tonnazh'] || $parameters['kp']) {
			$company->timestamps = false;
			$company->last_event_at = Carbon::now();
			$company->save();

			$event = new Event([
				'event_type_id' => 3,
				'title' => $parameters['kp'],
				'amount' => $parameters['tonnazh'],
				'created_user_id' => 1,
				'updated_user_id' => 1,
			]);
			$company->events()->save($event);
		}

		if ($parameters['comment']) {

			$company->timestamps = false;
			$company->last_event_at = Carbon::now();
			$company->save();

			$event = new Event([
				'event_type_id' => 1,
				'comment' => $parameters['comment'],
				'created_user_id' => 1,
				'updated_user_id' => 1,
			]);
			$company->events()->save($event);
		}

		// set company status

		if ($parameters['tonnazh']) {
			if ($parameters['tonnazh'] >= 1000) {
				$company->status_id = 1;
			} elseif ($parameters['tonnazh'] >= 500 && $parameters['tonnazh'] < 1000) {
				$company->status_id = 2;
			} elseif ($parameters['tonnazh'] >= 200 && $parameters['tonnazh'] < 500) {
				$company->status_id = 3;
			} elseif ($parameters['tonnazh'] >= 50 && $parameters['tonnazh'] < 200) {
				$company->status_id = 4;
			} elseif ($parameters['tonnazh'] >= 10 && $parameters['tonnazh'] < 50) {
				$company->status_id = 5;
			} else {
				$company->status_id = 6;
			}
			$company->save();
		}
	}

}

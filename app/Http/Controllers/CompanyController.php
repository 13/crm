<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Region;
use App\Models\CompanyName;
use App\Models\ContactPerson;
use App\Models\OrderSubject;
use App\Models\Source;
use App\Models\User;
use App\Models\Condition;
use App\Models\Event;
use App\Models\EventType;
use Auth;
use Validator;
use View;
use Carbon\Carbon;
use App\Libraries\String;

class CompanyController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function home()
	{
		$overdue_events = Event::overdue()->orderBy('event_type_id')->orderBy('scheduled_for')->get()->groupBy(function() {
			return 'Просрочка';
		});
		$today_events = Event::today()->orderBy('event_type_id')->orderBy('scheduled_for')->get()->groupBy(function() {
			return 'Сегодня';
		});
		$future_events = Event::permitted()->scheduled()->with('company')->orderBy('scheduled_for')->where('scheduled_for', '>=', Carbon::tomorrow())->get()->groupBy(function($item) {
			return Carbon::parse($item->scheduled_for)->format('d.m.Y');
		});
		$events = array_merge($overdue_events->all(), $today_events->all(), $future_events->all());
		return view('home', compact('events'));
	}

	public function getCreate()
	{
		$regions = Region::orderBy('title')->get();
		$sources = Source::orderBy('title')->get();
		$order_subjects = OrderSubject::orderBy('title')->get();
		$managers = User::withoutSysAdmins()->orderBy('name')->get();
		return view('companies.create', compact('regions', 'sources', 'order_subjects', 'managers'));
	}

	public function postCreate(Request $request)
	{
		$request->merge([
			'created_user_id' => Auth::user()->id,
			'updated_user_id' => Auth::user()->id,
		]);

		$request->merge(['confirmed_at' => Carbon::now()]);

		if (!Auth::user()->access_level->can_assign_manager) {
			$request->merge(['current_manager_id' => Auth::user()->id]);
		}

		if ('0' === $request->input('source_type')) {
			$request->merge(['request_number' => null]);
		}

		if ($request->has('region_new')) {
			$region = Region::create(array_merge([
				'title' => $request->input('region_new')
			], $request->only('created_user_id', 'updated_user_id')));
			$request->merge(['region_id' => $region->id]);
		}

		if ($request->has('source_new')) {
			$source = Source::create(array_merge([
				'title' => $request->input('source_new'),
				'is_incoming' => $request->input('source_type'),
			], $request->only('created_user_id', 'updated_user_id')));
			$request->merge(['source_id' => $source->id]);
		}

		if ($request->has('order_subject_new')) {
			$order_subject = OrderSubject::create(array_merge([
				'title' => $request->input('order_subject_new'),
			], $request->only('created_user_id', 'updated_user_id')));
			$request->merge(['order_subject_id' => $order_subject->id]);
		}

		foreach ($request->input('company_names') as $company_name) {
			if (!empty($company_name)) {
				$company_names[] = new CompanyName(['title' => $company_name]);
			}
		}
		foreach ($request->input('contact_persons') as $contact_person) {
			if (array_filter($contact_person)) {
				$contact_persons[] = new ContactPerson($contact_person);
			}
		}

		$company = Company::create(array_merge($request->all()));
		if (!empty($company_names)) {
			$company->names()->saveMany($company_names);
		}
		if (!empty($contact_persons)) {
			$company->contact_persons()->saveMany($contact_persons);
		}

		$company->managers()->create(['user_id' => $request->input('current_manager_id')]);

		if ($request->has('request')) {
			$order_subject = $company->events()->create(array_merge([
				'event_type_id' => EventType::where('slug', 'request')->first()->id,
				'comment' => $request->input('request'),
			], $request->only('created_user_id', 'updated_user_id')));
		}

		return redirect(action('CompanyController@getList'))->with([
			'added_company_id' => $company->id
		]);
	}

	public function getList(Request $request)
	{
		$query = Company::relatedToManager()->confirmed()->withoutArchivedAndRejected()->with(['names', 'manager', 'contact_persons', 'events_scheduled' => function($query) {
			$query->with('type');
		}, 'offers', 'events', 'condition'])->orderBy('created_at');
		$companies = self::processFilters($query, $request)->get();
		return view('companies.list-all', compact('companies'));
	}

	public function getListAll(Request $request)
	{
		$query = Company::permitted()->confirmed()->withoutArchivedAndRejected()->with(['names', 'manager', 'contact_persons', 'events_scheduled' => function($query) {
			$query->with('type');
		}, 'offers', 'events', 'condition'])->orderBy('created_at');
		$companies = self::processFilters($query, $request)->get();
		return view('companies.list-all', compact('companies'));
	}

	public function getListIn(Request $request)
	{
		$query = Company::permitted()->confirmed()->withoutArchivedAndRejected()->with(['names', 'manager', 'contact_persons', 'events_scheduled' => function($query) {
			$query->with('type');
		}, 'offers', 'events', 'condition'])->orderBy('request_number')->orderBy('created_at');
		$query->incoming();
		$companies = $query->get();
		$title = 'Входящие';
		return view('companies.list-custom', compact('companies', 'title'));
	}

	public function getListOut(Request $request)
	{
		$query = Company::permitted()->confirmed()->withoutArchivedAndRejected()->with(['names', 'manager', 'contact_persons', 'events_scheduled' => function($query) {
			$query->with('type');
		}, 'offers', 'events', 'condition'])->orderBy('created_at');
		$query->outgoing();
		$companies = $query->get();
		$title = 'Исходящие';
		return view('companies.list-custom', compact('companies', 'title'));
	}

	public function getListForgotten(Request $request)
	{
		$companies = Company::permitted()->confirmed()->withoutArchivedAndRejected()->forgotten()->with(['names', 'manager', 'contact_persons', 'events_scheduled' => function($query) {
			$query->with('type');
		}, 'offers', 'events', 'condition'])->orderBy('created_at')->get();
		$title = 'Забытые компании';
		return view('companies.list-custom', compact('companies', 'title'));
	}

	public function getListArchive(Request $request)
	{
		$companies = Company::permitted()->confirmed()->archivedAndRejected()->with(['names', 'manager', 'contact_persons', 'events_scheduled' => function($query) {
			$query->with('type');
		}, 'offers', 'events', 'condition'])->orderBy('created_at')->get();
		$title = 'Архив';
		return view('companies.list-custom', compact('companies', 'title'));
	}

	public function getView($id)
	{
		$company = Company::permitted()->find($id);
		return view('companies.view', compact('company'));
	}

	public function getEdit($id)
	{
		$company = Company::permitted()->find($id);
		$regions = Region::orderBy('title')->get();
		$sources = Source::orderBy('title')->get();
		$order_subjects = OrderSubject::orderBy('title')->get();
		$managers = User::withoutSysAdmins()->orderBy('name')->get();
		$conditions = Condition::orderBy('id')->get();
		return view('companies.edit', compact('company', 'conditions', 'regions', 'sources', 'order_subjects', 'managers'));
	}

	public function postEdit($company_id, Request $request)
	{
		$request->merge([
			'created_user_id' => Auth::user()->id,
			'updated_user_id' => Auth::user()->id,
		]);

		if ('0' === $request->input('source_type')) {
			$request->merge(['request_number' => null]);
		}

		if ($request->has('region_new')) {
			$region = Region::create(array_merge([
				'title' => $request->input('region_new')
			], $request->only('created_user_id', 'updated_user_id')));
			$request->merge(['region_id' => $region->id]);
		}

		if ($request->has('source_new')) {
			$source = Source::create(array_merge([
				'title' => $request->input('source_new'),
				'is_incoming' => $request->input('source_type'),
			], $request->only('created_user_id', 'updated_user_id')));
			$request->merge(['source_id' => $source->id]);
		}

		if ($request->has('order_subject_new')) {
			$order_subject = OrderSubject::create(array_merge([
				'title' => $request->input('order_subject_new'),
			], $request->only('created_user_id', 'updated_user_id')));
			$request->merge(['order_subject_id' => $order_subject->id]);
		}

		if ($request->has('old_company_names')) {
			foreach ($request->input('old_company_names') as $id => $name) {
				$company_name = CompanyName::find($id);
				if ($company_name) {
					if ('' !== $name) {
						$company_name->update(['title' => $name]);
					} else {
						$company_name->delete();
					}
				}
			}
		}

		if ($request->has('old_contact_persons')) {
			foreach ($request->input('old_contact_persons') as $id => $person) {
				$contact_person = ContactPerson::find($id);
				if ($contact_person) {
					if (array_filter($person)) {
						$contact_person->update($person);
					} else {
						$contact_person->delete();
					}
				}
			}
		}

		foreach ($request->input('company_names') as $company_name) {
			if (!empty($company_name)) {
				$company_names[] = new CompanyName(['title' => $company_name]);
			}
		}
		foreach ($request->input('contact_persons') as $contact_person) {
			if (array_filter($contact_person)) {
				$contact_persons[] = new ContactPerson($contact_person);
			}
		}

		$company = Company::find($company_id);
		$company->update($request->except('created_user_id'));

		if (!empty($company_names)) {
			$company->names()->saveMany($company_names);
		}
		if (!empty($contact_persons)) {
			$company->contact_persons()->saveMany($contact_persons);
		}

		if (Auth::user()->access_level->can_assign_manager) {
			if ((int) $company->managers->last()->user->id !== (int) $request->input('current_manager_id')) {
				$company->managers->last()->update(['ended_at' => Carbon::now()]);
				$company->managers()->create(['user_id' => $request->input('current_manager_id')]);
			}
		}

		return redirect(action('CompanyController@getView', $company_id));
	}

	public function getConversation($id)
	{
		$company = Company::permitted()->find($id);

		$company->events = $company->events->filter(function($item) {
			if ($item->type) {
				return $item->type->show_in_conversation;
			}
		});

		$conditions = Condition::all();

		return view('companies.conversation', compact('company', 'conditions'));
	}

	public function postConversation($id, Request $request)
	{
		$request->merge([
			'created_user_id' => Auth::user()->id,
			'updated_user_id' => Auth::user()->id,
		]);

		if ($request->hasFile('file')) {
			$request->merge(['file_name' => $request->file('file')->getClientOriginalName()]);
		}

		$company = Company::permitted()->find($id);
		$company->timestamps = false;
		$company->last_event_at = Carbon::now();
		$company->save();

		$event = new Event($request->all());
		$company->events()->save($event);

		if ($request->hasFile('file')) {
			$request->file('file')->move(storage_path() . '/app/attachments/', $event->id);
		}

		if ('offer' === $event->type->slug && $event->amount) {
			if ($event->amount >= 1000) {
				$company->status_id = 1;
			} elseif ($event->amount >= 500 && $event->amount < 1000) {
				$company->status_id = 2;
			} elseif ($event->amount >= 200 && $event->amount < 500) {
				$company->status_id = 3;
			} elseif ($event->amount >= 50 && $event->amount < 200) {
				$company->status_id = 4;
			} elseif ($event->amount >= 10 && $event->amount < 50) {
				$company->status_id = 5;
			} else {
				$company->status_id = 6;
			}
			$company->save();
		}

		if (!$request->has('scheduled_for')) {
			$achieved_event = $company->events()->where('event_type_id', $request->input('event_type_id'))->scheduled()->first();
			if (null !== $achieved_event) {
				$achieved_event->achieved = 1;
				$achieved_event->save();
			}
		}

		return redirect()->back();
	}

	public function getAttachments($id)
	{
		$company = Company::permitted()->find($id);

		$company->events = $company->events->filter(function($item) {
			if ($item->type) {
				return $item->type->show_in_attachments && $item->file_name;
			}
		});

		return view('companies.attachments', compact('company'));
	}

	public function postCheckDuplicates(Request $request)
	{
		$matches = [];
		if ($request->has('website')) {
			$matches['Вебсайт'][$request->input('website')] = Company::with('manager')->get()->filter(function($company) use ($request) {
				return String::trimDomainName($company->website) === String::trimDomainName($request->input('website'));
			})->first();
		}
		foreach ($request->input('company_names') as $name) {
			if ($name !== '') {
				$matches['Имя'][$name] = Company::with('manager', 'names')->get()->filter(function($company) use ($name) {
					foreach ($company->names as $stored_name) {
						return String::cleanString($stored_name->title) === String::cleanString($name);
					}
					return false;
				})->first();
			}
		}
		foreach ($request->input('contact_persons') as $contact_person) {
			if ($contact_person['email'] !== '') {
				$matches['Email'][$contact_person['email']] = Company::with('manager')->whereHas('contact_persons', function($query) use ($contact_person) {
					$query->where('email', $contact_person['email']);
				})->first();
			}
			if ($contact_person['phone'] !== '') {
				$matches['Телефон'][$contact_person['phone']] = Company::with('manager')->whereHas('contact_persons', function($query) use ($contact_person) {
					$query->where('phone', '<>', '');
				})->get()->filter(function($company) use ($contact_person) {
					foreach ($company->contact_persons as $person) {
						return String::trimPhoneNumber($person->phone) === String::trimPhoneNumber($contact_person['phone']);
					}
					return false;
				})->first();
			}
		}
		return array_filter($matches, function(&$item) {
			$item = array_filter($item);
			return $item;
		});
	}

	public function postUpdateStatus(Request $request)
	{
		if ($request->has('company_id')) {
			Company::find($request->input('company_id'))->update([
				'updated_user_id' => Auth::user()->id,
				'condition_id' => $request->input('condition_id'),
			]);
		}
		return redirect()->back();
	}

	private static function processFilters($query, $request)
	{
		if ($request->has('managers')) {
			$query->whereIn('current_manager_id', $request->input('managers'));
		}
		if ($request->has('regions')) {
			$query->whereIn('region_id', $request->input('regions'));
		}
		if ($request->has('event_status') || $request->has('event_type') || $request->has('amount.from') || $request->has('amount.to') || $request->has('period.from') || $request->has('period.to')) {
			$query->whereHas('events', function($query) use ($request) {
				if ($request->has('event_status')) {
					if (1 === (int) $request->input('event_status')) {
						$query->scheduled();
					} else {
						$query->held();
					}
				}
				if ($request->has('event_type')) {
					$query->where('event_type_id', $request->input('event_type'));
				}
				if ($request->has('amount.from')) {
					$query->payment()->where('amount', '>=', $request->input('amount.from'));
				}
				if ($request->has('amount.to')) {
					$query->payment()->where('amount', '<=', $request->input('amount.to'));
				}
				if ($request->has('period.from')) {
					$query->where('created_at', '>=', Carbon::parse($request->input('period.from')));
				}
				if ($request->has('period.to')) {
					$query->where('created_at', '<=', Carbon::parse($request->input('period.to')));
				}
			});
		}
		if ($request->has('last_event_at')) {
			$query->where('last_event_at', '<=', Carbon::parse($request->input('last_event_at')));
		}
		if ($request->has('confirmed.from')) {
			$query->where('confirmed_at', '>=', Carbon::parse($request->input('confirmed.from')));
		}
		if ($request->has('confirmed.to')) {
			$query->where('confirmed_at', '<=', Carbon::parse($request->input('confirmed.to')));
		}
		if ($request->has('source_type')) {
			$query->whereHas('source', function($query) use ($request) {
				$query->where('is_incoming', $request->input('source_type'));
			});
		}
		if ($request->has('source_id')) {
			$query->where('source_id', $request->input('source_id'));
		}
		if ($request->has('order_subject_id')) {
			$query->where('order_subject_id', $request->input('order_subject_id'));
		}
		return $query;
	}
}

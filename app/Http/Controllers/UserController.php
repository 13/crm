<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Registrar;
use Auth;
use App\Models\User;
use App\Models\AccessLevel;

class UserController extends Controller {

	public function __construct(Registrar $registrar)
	{
		$this->registrar = $registrar;
		$this->middleware('auth');
		if (!Auth::user()->access_level->can_register_user) {
			return abort(404);
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('users.list', ['users' => User::orderBy('name')->get()]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return view('users.edit', ['user' => User::find($id), 'access_levels' => AccessLevel::all()]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$request->merge([
			'updated_user_id' => Auth::user()->id,
		]);
		if ($request->has('password')) {
			$request->merge([
				'password' => bcrypt($request->input('password')),
			]);
			$data = $request->all();
		} else {
			$data = $request->except('password');
		}
		User::find($id)->update($data);
		return redirect()->action('UserController@index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		User::destroy($id);
		return redirect()->back();
	}

}

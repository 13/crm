<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\Models\OrderSubject;

class OrderSubjectController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		if (!Auth::user()->access_level->can_edit_order_subjects) {
			return abort(404);
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('order_subjects.list', ['order_subjects' => OrderSubject::orderBy('title')->get()]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('order_subjects.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$request->merge(['created_user_id' => Auth::user()->id]);
		OrderSubject::create($request->all());
		return redirect()->action('OrderSubjectController@index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return view('order_subjects.edit', ['order_subject' => OrderSubject::find($id)]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$request->merge(['updated_user_id' => Auth::user()->id]);
		OrderSubject::find($id)->update($request->all());
		return redirect()->action('OrderSubjectController@index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		OrderSubject::destroy($id);
		return redirect()->back();
	}

}

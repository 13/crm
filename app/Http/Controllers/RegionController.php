<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\Models\Region;

class RegionController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		if (!Auth::user()->access_level->can_edit_regions) {
			return abort(404);
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('regions.list', ['regions' => Region::orderBy('title')->get()]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('regions.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$request->merge(['created_user_id' => Auth::user()->id]);
		Region::create($request->all());
		return redirect()->action('RegionController@index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return view('regions.edit', ['region' => Region::find($id)]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$request->merge(['updated_user_id' => Auth::user()->id]);
		Region::find($id)->update($request->all());
		return redirect()->action('RegionController@index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Region::destroy($id);
		return redirect()->back();
	}

}

<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Event;
use App\Models\EventType;
use Auth;
use Storage;
use Carbon\Carbon;

class EventController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function getCalls()
	{
		$events = Event::permitted()->scheduled()->calls()->with('company')->orderBy('scheduled_for')->get()->groupBy(function($item) {
			return Carbon::parse($item->scheduled_for)->format('d.m.Y');
		});
		$title = 'Звонки';
		return view('events.list', compact('events', 'title'));
	}

	public function getOffers()
	{
		$events = Event::permitted()->scheduled()->offers()->with('company')->orderBy('scheduled_for')->get()->groupBy(function($item) {
			return Carbon::parse($item->scheduled_for)->format('d.m.Y');
		});
		$title = 'КП';
		return view('events.list', compact('events', 'title'));
	}

	public function getMeetings()
	{
		$events = Event::permitted()->scheduled()->meetings()->with('company')->orderBy('scheduled_for')->get()->groupBy(function($item) {
			return Carbon::parse($item->scheduled_for)->format('d.m.Y');
		});
		$title = 'Встречи';
		return view('events.list', compact('events', 'title'));
	}

	public function getAttachment($id)
	{
		$event = Event::find($id);
		if (!is_null($event->file_name) && (Auth::user()->access_level->can_access_all_companies || $event->company->current_manager_id === Auth::user()->id) && Storage::exists('attachments/' . $id)) {
			return response()->download(storage_path() . '/app/attachments/' . $id, $event->file_name);
		}
		abort(404);
	}

	public function getEdit($id)
	{
		if (!Auth::user()->access_level->can_edit_events) {
			abort(404);
		}
		$event = Event::find($id);
		$companies = Company::permitted()->get();
		$event_types = EventType::all();
		return view('events.edit', compact('event', 'companies', 'event_types'));
	}

	public function postEdit($id, Request $request)
	{
		if (!Auth::user()->access_level->can_edit_events) {
			abort(404);
		}
		$request->merge(['updated_user_id' => Auth::user()->id]);
		if ($request->input('achieved') === 'null') {
			$request->merge(['achieved' => null]);
		}
		if ($request->hasFile('file')) {
			$request->merge(['file_name' => $request->file('file')->getClientOriginalName()]);
			$request->file('file')->move(storage_path() . '/app/attachments/', $id);
		}
		Event::find($id)->update($request->all());
		if ($request->has('url_previous')) {
			return redirect($request->input('url_previous'));
		}
		return redirect(action('CompanyController@getConversation', $request->input('company_id')));
	}

	public function deleteRemove($id)
	{
		if (!Auth::user()->access_level->can_edit_events) {
			abort(404);
		}
		$attached_file_path = 'attachments/' . $id;
		if (Storage::exists($attached_file_path)) {
			Storage::delete($attached_file_path);
		}
		Event::destroy($id);
		return redirect()->back();
	}
}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\Models\Source;

class SourceController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		if (!Auth::user()->access_level->can_edit_sources) {
			return abort(404);
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('sources.list', ['sources_grouped' => Source::orderBy('title')->get()->groupBy('is_incoming')]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('sources.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$request->merge(['created_user_id' => Auth::user()->id]);
		Source::create($request->all());
		return redirect()->action('SourceController@index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return view('sources.edit', ['source' => Source::find($id)]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$request->merge(['updated_user_id' => Auth::user()->id]);
		Source::find($id)->update($request->all());
		return redirect()->action('SourceController@index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Source::destroy($id);
		return redirect()->back();
	}

}

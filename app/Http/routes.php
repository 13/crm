<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'CompanyController@home');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	'company' => 'CompanyController',
	'event' => 'EventController',
]);

Route::resources([
	'regions' => 'RegionController',
	'subjects' => 'OrderSubjectController',
	'sources' => 'SourceController',
	'users' => 'UserController',
]);

// Event::listen('illuminate.query', function($query)
// {
// 	var_dump($query);
// });

<?php namespace App\Libraries;

class String {

	public static function cleanString($string)
	{
		return preg_replace('/[^\p{L}0-9]/u', '', mb_strtolower($string, 'UTF-8'));
	}

	public static function trimPhoneNumber($string)
	{
		return substr(preg_replace('/[^0-9]/', '', $string), -10);
	}

	public static function trimDomainName($string)
	{
		return preg_replace('/^(?:.*\/\/|)(?:www\.|)(.+?)(?:\/.*|)$/u', '\1', mb_strtolower($string, 'UTF-8'));
	}
}

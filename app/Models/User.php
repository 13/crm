<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'access_level_id', 'created_user_id', 'updated_user_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function access_level()
	{
		return $this->belongsTo('App\Models\AccessLevel');
	}

	public function created_user()
	{
		return $this->belongsTo('App\Models\User', 'created_user_id');
	}

	public function updated_user()
	{
		return $this->belongsTo('App\Models\User', 'updated_user_id');
	}

	public function scopeWithoutSysAdmins($query)
	{
		$query->where('access_level_id', '!=', '4');
	}

}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ManagersHistory extends Model {

	protected $table = 'managers_history';

	protected $fillable = array('user_id', 'company_id', 'ended_at');

	protected $dates = ['ended_at'];

	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}

}

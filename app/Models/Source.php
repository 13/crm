<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Source extends Model {

	protected $fillable = array('title', 'is_incoming', 'created_user_id', 'updated_user_id');

	public function created_user()
	{
		return $this->belongsTo('App\Models\User', 'created_user_id');
	}

	public function updated_user()
	{
		return $this->belongsTo('App\Models\User', 'updated_user_id');
	}

}

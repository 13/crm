<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;

class Company extends Model {

	protected $fillable = array(
		'region_id',
		'request_number',
		'current_manager_id',
		'email',
		'website',
		'address',
		'source_id',
		'order_subject_id',
		'status_id',
		'created_user_id',
		'updated_user_id',
		'description',
		'requisites',
		'confirmed_at',
		'condition_id',
	);

	protected $dates = ['confirmed_at', 'last_event_at'];

	public function names()
	{
		return $this->hasMany('App\Models\CompanyName');
	}

	public function region()
	{
		return $this->belongsTo('App\Models\Region');
	}

	public function manager()
	{
		return $this->belongsTo('App\Models\User', 'current_manager_id');
	}

	public function managers()
	{
		return $this->hasMany('App\Models\ManagersHistory');
	}

	public function contact_persons()
	{
		return $this->hasMany('App\Models\ContactPerson');
	}

	public function source()
	{
		return $this->belongsTo('App\Models\Source');
	}

	public function order_subject()
	{
		return $this->belongsTo('App\Models\OrderSubject');
	}

	public function status()
	{
		return $this->belongsTo('App\Models\CompanyStatus');
	}

	public function condition()
	{
		return $this->belongsTo('App\Models\Condition');
	}

	public function created_user()
	{
		return $this->belongsTo('App\Models\User');
	}

	public function updated_user()
	{
		return $this->belongsTo('App\Models\User');
	}

	public function events()
	{
		return $this->hasMany('App\Models\Event');
	}

	public function events_scheduled()
	{
		return $this->hasMany('App\Models\Event')->offersCallsMeetings()->scheduled();
	}

	public function offers()
	{
		return $this->hasMany('App\Models\Event')->offers()->held();
	}

	public function comma_separated_names()
	{
		$names = [];
		foreach ($this->names as $name) {
			$names[] = $name->title;
		}
		$names = implode(', ', $names);
		if ('' === $names) {
			$names = '(Без названия)';
		}
		$names_result['full'] = $names_result['short'] = $names;
		if (mb_strlen($names) > 30) {
			$names_result['short'] = mb_substr($names, 0, 30) . '...';
		}
		return $names_result;
	}

	public function last_offer_amount()
	{
		if (count($this->offers)) {
			return $this->offers->last()->amount;
		}
	}

	public function last_offer_title()
	{
		if (count($this->offers)) {
			return $this->offers->last()->title;
		}
	}

	public function scopePermitted($query)
	{
		if (!Auth::user()->access_level->can_access_all_companies) {
			$query->where('current_manager_id', Auth::user()->id);
		}
	}

	public function scopeRelatedToManager($query)
	{
		$query->where('current_manager_id', Auth::user()->id);
	}

	public function scopeConfirmed($query)
	{
		$query->whereNotNull('confirmed_at');
	}

	public function scopeIncoming($query)
	{
		$query->whereNotNull('request_number');
	}

	public function scopeOutgoing($query)
	{
		$query->whereNull('request_number');
	}

	public function scopeWithoutArchivedAndRejected($query)
	{
		$query->whereNotIn('condition_id', [1, 2]);
	}

	public function scopeArchivedAndRejected($query)
	{
		$query->whereIn('condition_id', [1, 2]);
	}

	public function scopeForgotten($query)
	{
		$query->where('last_event_at', '<=', Carbon::now()->subWeeks(3)); // не активны 3 недели
	}

	public function contact_person()
	{
		if ($this->contact_persons) {
			return $this->contact_persons->first();
		}
	}

	public function contact_name()
	{
		if ($this->contact_person()) {
			return $this->contact_person()->name;
		}
	}

	public function contact_phone()
	{
		if ($this->contact_person()) {
			return $this->contact_person()->phone;
		}
	}

	public function contact_email()
	{
		if ($this->contact_person()) {
			return $this->contact_person()->email;
		}
	}

	public function getCommaSeparatedNamesAttribute()
	{
	    return $this->comma_separated_names();
	}

	protected $appends = ['comma_separated_names'];

	public function last_comments($number)
	{
		$events = $this->events->reverse();
		$result_events = [];
		foreach ($events as $event) {
			if (trim($event->comment) !== '') {
				array_unshift($result_events, ['date' => $event->created_at, 'text' => $event->comment]);
			}
			if ($number === count($result_events)) {
				break;
			}
		}
		return $result_events;
	}
}

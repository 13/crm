<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyName extends Model {

	protected $fillable = array('title');

	public $timestamps = false;

}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderSubject extends Model {

	protected $fillable = array(
		'title',
		'created_user_id',
		'updated_user_id'
	);

	public function created_user()
	{
		return $this->belongsTo('App\Models\User', 'created_user_id');
	}

	public function updated_user()
	{
		return $this->belongsTo('App\Models\User', 'updated_user_id');
	}

}

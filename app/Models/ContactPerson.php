<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactPerson extends Model {

	public $timestamps = false;

	protected $table = 'contact_persons';

	protected $fillable = array(
		'name',
		'position',
		'phone',
		'email',
	);

}

<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Event extends Model {

	protected $fillable = array(
		'event_type_id',
		'title',
		'scheduled_for',
		'achieved',
		'file_name',
		'amount',
		'comment',
		'created_user_id',
		'updated_user_id',
	);

	protected $dates = ['scheduled_for'];

	public function type()
	{
		return $this->belongsTo('App\Models\EventType', 'event_type_id');
	}

	public function company()
	{
		return $this->belongsTo('App\Models\Company');
	}

	public function manager()
	{
		return $this->belongsTo('App\Models\User', 'created_user_id');
	}

	public function setScheduledForAttribute($datetime)
	{
		$this->attributes['scheduled_for'] = trim($datetime) === ''? null : Carbon::createFromFormat('d.m.Y H:i', $datetime);
	}

	public function scopePermitted($query)
	{
		if (!Auth::user()->access_level->can_access_all_companies) {
			$query->whereHas('company', function($query) {
				$query->where('current_manager_id', Auth::user()->id);
			});
		}
	}

	public function scopeScheduled($query)
	{
		$query->whereNotNull('scheduled_for')->whereNull('achieved');
	}

	public function scopeOffersCallsMeetings($query)
	{
		$query->whereIn('event_type_id', [1, 2, 3]);
	}

	public function scopeHeld($query)
	{
		$query->whereNull('scheduled_for');
	}

	public function scopeOverdue($query)
	{
		$query->permitted()->whereNull('achieved')->where('scheduled_for', '<', Carbon::today());
	}

	public function scopeToday($query)
	{
		$query->permitted()->whereNull('achieved')->where('scheduled_for', '>=', Carbon::today())->where('scheduled_for', '<', Carbon::tomorrow());
	}

	public function scopeOffers($query)
	{
		$query->where('event_type_id', '3')/*->whereNotNull('file_name')*/;
	}

	public function scopeCalls($query)
	{
		$query->where('event_type_id', '1');
	}

	public function scopeMeetings($query)
	{
		$query->where('event_type_id', '2');
	}

	public function scopePayment($query)
	{
		$query->where('event_type_id', '5');
	}

	public function status()
	{
		if ($this->scheduled_for) {
			if ($this->achieved) {
				return 'achieved';
			}
			return 'scheduled';
		}
		return 'info';
	}
}

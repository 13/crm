<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Company;
use App\Models\Event;
use App\Models\User;
use App\Models\Region;
use App\Models\EventType;
use App\Models\Source;
use App\Models\OrderSubject;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		view()->composer('partials.nav', function($view) {
			$view->with([
				'count' => [
					'companies' => count(Company::relatedToManager()->confirmed()->withoutArchivedAndRejected()->get()),
					'companies-all' => count(Company::permitted()->confirmed()->withoutArchivedAndRejected()->get()),
					'companies-in' => count(Company::permitted()->confirmed()->withoutArchivedAndRejected()->incoming()->get()),
					'companies-out' => count(Company::permitted()->confirmed()->withoutArchivedAndRejected()->outgoing()->get()),
					'companies-forgotten' => count(Company::permitted()->confirmed()->withoutArchivedAndRejected()->forgotten()->get()),
					'companies-archive' => count(Company::permitted()->confirmed()->archivedAndRejected()->get()),
					'calls' => count(Event::permitted()->scheduled()->calls()->get()),
					'offers' => count(Event::permitted()->scheduled()->offers()->get()),
					'meetings' => count(Event::permitted()->scheduled()->meetings()->get()),
				],
			]);
		});
		view()->composer('companies.partials.filters', function($view) {
			$view->with([
				'managers' => User::withoutSysAdmins()->orderBy('name')->get(),
				'regions' => Region::orderBy('title')->get(),
				'event_types' => EventType::orderBy('title')->get(),
				'sources' => Source::orderBy('title')->get(),
				'order_subjects' => OrderSubject::orderBy('title')->get(),
			]);
		});
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}

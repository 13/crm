(function(){
	var index = 0;
	$('#add-company-name').on('click', function(event){
		var group = $("#company-name-group");
		var clone = group.clone();
		var label = clone.find(".control-label");
		clone.find("input").val("");
		clone.attr('id', group.attr('id') + '-' + (++index));
		label.html(label.html() + " (" + (index + 1) + ")");
		$(this).parent().parent().before(clone);
		event.preventDefault();
	});
})();

(function(){
	var index = 0;
	$('#add-contact-person').on('click', function(event){
		var group = $("#contact-person-group");
		var clone = group.clone();
		var groupTitle = clone.find(".contact-person-group-title");
		clone.find("input").val("");
		clone.attr('id', group.attr('id') + '-' + (++index));
		groupTitle.html(groupTitle.html() + " (" + (index + 1) + ")");
		clone.find("input").attr("name", function(){
			return $(this).attr("name").replace("contact_persons[0]", "contact_persons[" + index + "]");
		});
		$(this).parent().parent().before(clone);
		event.preventDefault();
	});
})();

$('#add-region').on('click', function(event){
	$('<input class="form-control" name="region_new">').replaceAll('select[name="region_id"]').focus();
	$(this).parent().parent().remove();
	event.preventDefault();
});
$('#add-order-subject').on('click', function(event){
	$('<input class="form-control" name="order_subject_new">').replaceAll('select[name="order_subject_id"]').focus();
	$(this).parent().parent().remove();
	event.preventDefault();
});
if ('' !== $('select[name="source_type"]').val()) {
	$('select[name="source_id"]')
		.find('option[data-is-incoming="' + +!+$('select[name="source_type"]').val() + '"]')
		.hide();
}
if (!$('select[name="source_id"]').val()) {
	$('select[name="source_id"]')
		.find('option')
		.filter(function(){
			return $(this).css('display') != 'none';
		})
		.first()
		.attr('selected', true);
};
$('select[name="source_type"]').on('change', function(){
	if ($(this).val() === '') {
		$('select[name="source_id"]')
			.find('option')
			.show();
	};
	if ($(this).val() === '0') {
		$('select[name="source_id"]')
			.find('option[data-is-incoming="1"]')
			.hide();
		$('select[name="source_id"]')
			.find('option[data-is-incoming="0"]')
			.show();
	};
	if ($(this).val() === '1') {
		$('select[name="source_id"]')
			.find('option[data-is-incoming="0"]')
			.hide();
		$('select[name="source_id"]')
			.find('option[data-is-incoming="1"]')
			.show();
	};
	$('select[name="source_id"]')
		.find('option')
		.filter(function(){
			return $(this).css('display') != 'none';
		})
		.first()
		.attr('selected', true);
});
$('#add-source').on('click', function(event){
	$('<input class="form-control" name="source_new">').replaceAll('select[name="source_id"]').focus();
	$(this).parent().parent().remove();
	event.preventDefault();
});
$('#create-company').on('submit', function(event){
	$(this).find('[type="submit"]').attr('disabled', true);
	$.post("/company/check-duplicates", $("form#create-company").serialize(), function(matches){
		if (!$.isEmptyObject(matches)) {
			$('#notice-modal').modal();
			var contentBlock = $('#notice-modal').find('.modal-body');
			contentBlock.empty();
			$.each(matches, function(key, match){
				contentBlock.append($('<h4/>', { text: key }));
				$.each(match, function(input, matchChild){
					contentBlock.append($('<p/>', { text: '«' + input + '» — компания «' + matchChild.comma_separated_names.short + '» (Менеджер: ' + matchChild.manager.name + ')' }));
				});
			});
			$('#create-company').find('[type="submit"]').attr('disabled', false);
		} else {
			$('#create-company')[0].submit();
		};
	});
	event.preventDefault();
});

// conversation
if ($("#conversation-list").length) {
	$("#conversation-list").scrollTop($("#conversation-list")[0].scrollHeight);
};

$('.datepicker').datetimepicker({
	lang: 'ru',
	dayOfWeekStart: '1',
	format:'d.m.Y H:i',
	onClose: function(){
		if ($('#conversation-form').length) {
			$('#conversation-form').find('input.datepicker').blur();
			$('#conversation-form').find('div.comment-block').find('textarea').focus();
		};
	},
});

$('#conversation-form').on('click', 'input[type="radio"]', function(){
	var name = $(this).attr('class');
	$('#conversation-form').find('div.input-block').find('label').addClass('disabled');
	$(this).parent().addClass('active').removeClass('disabled');
	if ('scheduled' == name) {
		$(this).closest('div.input-block').find('input.datepicker').attr('disabled', false).focus();
	} else if ('happened' == name) {
		$('#conversation-form').find('div.comment-block').find('textarea, button').attr('disabled', false);
		var numberInput = $(this).closest('div.input-block').find('input[type="number"]');
		if (numberInput.length) {
			numberInput.attr('disabled', false).focus();
		} else {
			$('#conversation-form').find('div.comment-block').find('textarea').focus();
		};
	};
});

$('#conversation-form').on('change', 'input.datepicker', function(){
	$('#conversation-form').find('div.comment-block').find('textarea, button').attr('disabled', false);
});

$('#conversation-form').on('change', 'input:file', function(){
	$('#conversation-form').find('input:file').not(this).attr('disabled', true);
	$(this).closest('div.input-block').find('input[type="radio"]').prop("checked", true);
	$('#conversation-form').find('div.input-block').find('label').addClass('disabled');
	$(this).parent().find('label').removeClass('disabled');
	$(this).closest('div.input-block').find('input[type="number"], input[type="text"]').attr('disabled', false).eq(1).focus();
	$('#conversation-form').find('div.comment-block').find('textarea, button').attr('disabled', false);
});

//attachments
$('#attachments-form').on('change', 'input:file', function(){
	$('#attachments-form').find('[disabled]').attr('disabled', false);
});

//company list
// $('tr').on("click", function() {
// 	if($(this).find('a').attr('href') !== undefined) {
// 		document.location = $(this).find('a').attr('href');
// 	}
// });

// home
$('.company-name').popover({
	trigger: 'hover',
	placement: 'auto',
	html: true
});

// modal
$('.confirm-delete').on('submit', function(event){
	var form = $(this);
	var modalBox = $('#confirm-modal');
	modalBox.modal();
	modalBox.find('#yes').focus();
	modalBox.on('click', '#yes', function(){
		form[0].submit();
	});
    modalBox.keypress(function(event){
        if(event.which == 13){
            $(this).find('#yes').click();
        }
    });
	event.preventDefault();
});

// stick thead
$('table.with-floating-header').floatThead();

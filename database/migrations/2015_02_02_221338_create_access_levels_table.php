<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessLevelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('access_levels', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->text('description');
			$table->boolean('can_access_all_companies');
			$table->boolean('can_access_all_events');
			$table->boolean('can_export_all_companies');
			$table->boolean('can_assign_manager');
			$table->boolean('can_register_user');
			$table->boolean('can_edit_events');
			$table->boolean('can_edit_regions');
			$table->boolean('can_edit_sources');
			$table->boolean('can_edit_order_subjects');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('access_levels');
	}

}

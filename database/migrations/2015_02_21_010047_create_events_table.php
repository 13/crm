<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id')->unsigned();
			$table->integer('event_type_id')->unsigned();
			$table->string('title')->nullable();
			$table->timestamp('scheduled_for')->nullable();
			$table->string('file_name')->nullable();
			$table->float('amount')->unsigned()->nullable();
			$table->boolean('achieved')->nullable();
			$table->text('comment');
			$table->integer('created_user_id')->unsigned();
			$table->integer('updated_user_id')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}

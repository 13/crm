<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourcesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sources', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->boolean('is_incoming');
			$table->unique(['title', 'is_incoming']);
			$table->timestamps();
			$table->integer('created_user_id')->unsigned();
			$table->integer('updated_user_id')->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sources');
	}

}

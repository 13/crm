<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('request_number')->unsigned()->nullable();
			$table->integer('current_manager_id')->unsigned();
			$table->integer('region_id')->unsigned();
			$table->text('address');
			$table->string('website');
			$table->text('description');
			$table->text('requisites');
			$table->integer('source_id')->unsigned();
			$table->integer('order_subject_id')->unsigned();
			$table->integer('status_id')->unsigned();
			$table->boolean('is_rejected');
			$table->boolean('in_archive');
			$table->timestamp('confirmed_at')->nullable();
			$table->timestamp('last_event_at')->nullable();
			$table->integer('created_user_id')->unsigned();
			$table->integer('updated_user_id')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}

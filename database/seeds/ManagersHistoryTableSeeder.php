<?php

use Illuminate\Database\Seeder;
use App\Models\ManagersHistory;

class ManagersHistoryTableSeeder extends Seeder {

	public function run()
	{
		ManagersHistory::truncate();
		ManagersHistory::create([
			'user_id' => 1,
			'company_id' => 1,
		]);
		ManagersHistory::create([
			'user_id' => 1,
			'company_id' => 2,
		]);
		ManagersHistory::create([
			'user_id' => 2,
			'company_id' => 2,
		]);
		ManagersHistory::create([
			'user_id' => 3,
			'company_id' => 3,
		]);
		ManagersHistory::create([
			'user_id' => 2,
			'company_id' => 4,
		]);
		ManagersHistory::create([
			'user_id' => 3,
			'company_id' => 1,
		]);
	}

}

<?php

use Illuminate\Database\Seeder;
use App\Models\CompanyName;

class CompanyNameTableSeeder extends Seeder {

	public function run()
	{
		CompanyName::truncate();
		CompanyName::create([
			'title' => 'ООО "ТрансМашБаш"',
			'company_id' => 1,
		]);
		CompanyName::create([
			'title' => 'ГРПБ АУОК "Транспортно-строительная управляющая компания"',
			'company_id' => 1,
		]);
		CompanyName::create([
			'title' => 'ООО "ЭкоСтройБрестТрестИнвест"',
			'company_id' => 1,
			'is_actual' => 0
		]);
		CompanyName::create([
			'title' => 'ООО "ТулМетОблСтрой"',
			'company_id' => 2,
			'is_actual' => 0
		]);
		CompanyName::create([
			'title' => 'ООО "ВозТашТранс"',
			'company_id' => 2,
		]);
	}

}

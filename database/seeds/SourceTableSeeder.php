<?php

use Illuminate\Database\Seeder;
use App\Models\Source;

class SourceTableSeeder extends Seeder {

	public function run()
	{
		Source::truncate();
		Source::create([
			'title' => 'Звонок',
			'is_incoming' => 1,
			'created_user_id' => 1,
		]);
		Source::create([
			'title' => 'E-mail',
			'is_incoming' => 1,
			'created_user_id' => 1,
		]);
		Source::create([
			'title' => 'С сайта',
			'is_incoming' => 1,
			'created_user_id' => 1,
		]);
		Source::create([
			'title' => 'По рекомендации от клиента',
			'is_incoming' => 1,
			'created_user_id' => 1,
		]);
		Source::create([
			'title' => 'Тендер',
			'is_incoming' => 1,
			'created_user_id' => 1,
		]);
		Source::create([
			'title' => 'Выставка',
			'is_incoming' => 1,
			'created_user_id' => 1,
		]);
		Source::create([
			'title' => 'СРО',
			'is_incoming' => 0,
			'created_user_id' => 1,
		]);
		Source::create([
			'title' => 'Интернет',
			'is_incoming' => 0,
			'created_user_id' => 1,
		]);
		Source::create([
			'title' => 'Тендерные площадки',
			'is_incoming' => 0,
			'created_user_id' => 1,
		]);
	}

}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('AccessLevelTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('SourceTableSeeder');
		$this->call('CompanyStatusTableSeeder');
		$this->call('ConditionTableSeeder');
		// $this->call('CompanyTableSeeder');
		// $this->call('ManagersHistoryTableSeeder');
		// $this->call('ContactPersonTableSeeder');
		$this->call('OrderSubjectTableSeeder');
		// $this->call('CompanyNameTableSeeder');
		$this->call('RegionTableSeeder');
		$this->call('EventTypeTableSeeder');
		// $this->call('EventTableSeeder');
	}

}

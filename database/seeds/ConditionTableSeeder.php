<?php

use Illuminate\Database\Seeder;
use App\Models\Condition;

class ConditionTableSeeder extends Seeder {

	public function run()
	{
		Condition::truncate();
		Condition::create([
			'title' => 'Отказ',
		]);
		Condition::create([
			'title' => 'Архив',
		]);
		Condition::create([
			'title' => 'Ждем ответ от...',
		]);
		Condition::create([
			'title' => 'Холодные',
		]);
		Condition::create([
			'title' => 'В работе',
		]);
		Condition::create([
			'title' => 'Теплые',
		]);
		Condition::create([
			'title' => 'Согласование условий',
		]);
		Condition::create([
			'title' => 'Встреча, договор',
		]);
		Condition::create([
			'title' => 'Стал клиентом',
		]);
	}

}

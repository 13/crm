<?php

use Illuminate\Database\Seeder;
use App\Models\Event;

class EventTableSeeder extends Seeder {

	public function run()
	{
		Event::truncate();
		Event::create([
			'company_id' => 2,
			'event_type_id' => 1,
			'scheduled_for' => '04.03.2015 11:00',
			'comment' => 'Очень важный звонок',
			'created_user_id' => 4,
			'updated_user_id' => 4,
		]);
		Event::create([
			'company_id' => 2,
			'event_type_id' => 1,
			'created_user_id' => 4,
			'updated_user_id' => 4,
		]);
		Event::create([
			'company_id' => 2,
			'event_type_id' => 3,
			'comment' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
			'created_user_id' => 4,
			'updated_user_id' => 4,
		]);
	}

}

<?php

use Illuminate\Database\Seeder;
use App\Models\AccessLevel;

class AccessLevelTableSeeder extends Seeder {

	public function run()
	{
		AccessLevel::truncate();
		AccessLevel::create([
			'title' => 'Менеджер',
		]);
		AccessLevel::create([
			'title' => 'Коммерческий директор',
			'description' => 'Руководитель отдела',
			'can_access_all_companies' => 1,
			'can_access_all_events' => 1,
			'can_assign_manager' => 1,
			'can_register_user' => 1,
			'can_edit_events' => 1,
			'can_edit_regions' => 1,
			'can_edit_sources' => 1,
			'can_edit_order_subjects' => 1,
		]);
		AccessLevel::create([
			'title' => 'Генеральный директор',
			'can_access_all_companies' => 1,
			'can_access_all_events' => 1,
			'can_assign_manager' => 1,
			'can_register_user' => 1,
			'can_edit_events' => 1,
			'can_edit_regions' => 1,
			'can_edit_sources' => 1,
			'can_edit_order_subjects' => 1,
			'can_export_all_companies' => 1,
		]);
		AccessLevel::create([
			'title' => 'Системный администратор',
			'can_access_all_companies' => 1,
			'can_access_all_events' => 1,
			'can_assign_manager' => 1,
			'can_register_user' => 1,
			'can_edit_events' => 1,
			'can_edit_regions' => 1,
			'can_edit_sources' => 1,
			'can_edit_order_subjects' => 1,
			'can_export_all_companies' => 1,
		]);
	}

}

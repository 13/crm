<?php

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanyTableSeeder extends Seeder {

	public function run()
	{
		Company::truncate();
		Company::create([
			'region_id' => 1,
			'address' => ' г. Тула, ул. Марата дом 100 офис 101',
			'current_manager_id' => 1,
			'website' => 'test.com',
			'source_id' => 1,
			'order_subject_id' => 1,
			'status_id' => 2,
			'created_user_id' => 1,
			'updated_user_id' => 1,
			'description' => 'Компания Неотек давно и успешно занимается реализацией хлопчатобумажных тканей, фурнитуры и текстильных изделий для дома через собственную сеть торговых представительств. На сегодняшний день на территории России работает 53 представительства и оптовых склада, а на Украине 11 представительств.',
			'requisites' => 'Расчетный счет 30301810000006000001
			Кор. счет 30101810400000000225 в Операционном управлении Главного управления Центрального банка Российской Федерации по Центральному федеральному округу г. Москва (ОПЕРУ Москва)
			БИК 044525225
			КПП 775001001
			ИНН 7707083893',
			'confirmed_at' => \Carbon\Carbon::now(),
			'last_event_at' => \Carbon\Carbon::now(),
		]);
	}

}

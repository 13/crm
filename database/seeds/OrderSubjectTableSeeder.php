<?php

use Illuminate\Database\Seeder;
use App\Models\OrderSubject;

class OrderSubjectTableSeeder extends Seeder {

	public function run()
	{
		OrderSubject::truncate();
		OrderSubject::create([
			'title' => 'ЛМК',
			'created_user_id' => 1,
		]);
	}

}

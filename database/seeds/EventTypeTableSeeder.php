<?php

use Illuminate\Database\Seeder;
use App\Models\EventType;

class EventTypeTableSeeder extends Seeder {

	public function run()
	{
		EventType::truncate();
		EventType::create([
			'title' => 'Звонок',
			'slug' => 'call',
			'show_in_conversation' => 1,
			'show_in_attachments' => 0,
		]);
		EventType::create([
			'title' => 'Встреча',
			'slug' => 'meeting',
			'show_in_conversation' => 1,
			'show_in_attachments' => 0,
		]);
		EventType::create([
			'title' => 'КП',
			'slug' => 'offer',
			'show_in_conversation' => 1,
			'show_in_attachments' => 1,
		]);
		EventType::create([
			'title' => 'Договор',
			'slug' => 'contract',
			'show_in_conversation' => 1,
			'show_in_attachments' => 1,
		]);
		EventType::create([
			'title' => 'Оплата',
			'slug' => 'payment',
			'show_in_conversation' => 1,
			'show_in_attachments' => 0,
		]);
		EventType::create([
			'title' => 'Файл',
			'slug' => 'file',
			'show_in_conversation' => 0,
			'show_in_attachments' => 1,
		]);
		EventType::create([
			'title' => 'Отказ',
			'slug' => 'reject',
			'show_in_conversation' => 1,
			'show_in_attachments' => 0,
		]);
		EventType::create([
			'title' => 'Архив',
			'slug' => 'archive',
			'show_in_conversation' => 1,
			'show_in_attachments' => 0,
		]);
		EventType::create([
			'title' => 'Запрос',
			'slug' => 'request',
			'show_in_conversation' => 1,
			'show_in_attachments' => 0,
		]);
	}

}

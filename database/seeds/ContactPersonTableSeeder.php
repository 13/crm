<?php

use Illuminate\Database\Seeder;
use App\Models\ContactPerson;

class ContactPersonTableSeeder extends Seeder {

	public function run()
	{
		ContactPerson::truncate();
		ContactPerson::create([
			'name' => 'Александр Ковальчук',
			'position' => 'Помощник младшего менеджера',
			'phone' => '+7 000 000 00 00',
			'email' => 'maslov@test-mail-server.com',
			'company_id' => 1,
		]);
		ContactPerson::create([
			'name' => 'Алексей',
			'position' => 'Исполнительный директор',
			'phone' => '8 000 1230000',
			'email' => 'aleksey@test.com',
			'company_id' => 1,
		]);
		ContactPerson::create([
			'name' => 'Борис Сечин',
			'position' => 'Менеджер',
			'phone' => '79998884466',
			'email' => 'boris@test.com',
			'company_id' => 2,
			'is_actual' => 0,
		]);
		ContactPerson::create([
			'name' => 'Андрей',
			'position' => 'Менеджер',
			'phone' => '+7-123-456-00-00',
			'email' => 'testemail@test.com',
			'company_id' => 3,
		]);
	}

}

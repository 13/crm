<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder {

	public function run()
	{
		User::truncate();
		User::create([
			'name' => 'Руководитель',
			'email' => '345245@test.com',
			'password' => '$2y$10$Z9fclYwkHjmA8PTiRPl1fOElEo4x1i3TEnYodDUYr0U3GxHUsYDVO',
			'access_level_id' => 2
		]);
		// User::create([
		// 	'name' => 'Константин Акимов',
		// 	'email' => 'konst.akimov@gmail.com',
		// 	'password' => '$2y$10$e3zptwUbWm9HNPLL6RlI8e811u958j0ot9VyadSSXNfYg0jJ.DlJu',
		// 	'access_level_id' => 4
		// ]);
		User::create([
			'name' => 'Данилина',
			'email' => '2546745@test.com',
			'password' => '$2y$10$Z9fclYwkHjmA8PTiRPl1fOElEo4x1i3TEnYodDUYr0U3GxHUsYDVO',
		]);
		User::create([
			'name' => 'Панкратьев',
			'email' => '63573@test.com',
			'password' => '$2y$10$Z9fclYwkHjmA8PTiRPl1fOElEo4x1i3TEnYodDUYr0U3GxHUsYDVO',
		]);
		User::create([
			'name' => 'Пахомов',
			'email' => '285626@test.com',
			'password' => '$2y$10$Z9fclYwkHjmA8PTiRPl1fOElEo4x1i3TEnYodDUYr0U3GxHUsYDVO',
		]);
		User::create([
			'name' => 'Рыжкова Елена',
			'email' => '9836@test.com',
			'password' => '$2y$10$Z9fclYwkHjmA8PTiRPl1fOElEo4x1i3TEnYodDUYr0U3GxHUsYDVO',
		]);
		User::create([
			'name' => 'Тимофеев',
			'email' => '286576788@test.com',
			'password' => '$2y$10$Z9fclYwkHjmA8PTiRPl1fOElEo4x1i3TEnYodDUYr0U3GxHUsYDVO',
		]);
	}

}

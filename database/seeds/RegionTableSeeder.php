<?php

use Illuminate\Database\Seeder;
use App\Models\Region;

class RegionTableSeeder extends Seeder {

	public function run()
	{
		Region::truncate();
		Region::create([
			'title' => 'Тульская область',
			'created_user_id' => 1,
		]);
		Region::create([
			'title' => 'Рязанская область',
			'created_user_id' => 1,
		]);
		Region::create([
			'title' => 'Калужская область',
			'created_user_id' => 1,
		]);
	}

}

<?php

use Illuminate\Database\Seeder;
use App\Models\CompanyStatus;

class CompanyStatusTableSeeder extends Seeder {

	public function run()
	{
		CompanyStatus::truncate();
		CompanyStatus::create([
			'title' => 'Золотая',
		]);
		CompanyStatus::create([
			'title' => 'Серебро',
		]);
		CompanyStatus::create([
			'title' => 'Бронза',
		]);
		CompanyStatus::create([
			'title' => 'Обычные',
		]);
		CompanyStatus::create([
			'title' => 'Мелкие',
		]);
		CompanyStatus::create([
			'title' => 'Трэш',
		]);
	}

}

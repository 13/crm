@extends('layout')

@section('content')
<div class="panel-heading">Редактирование события</div>
<div class="panel-body">
	<form class="form-horizontal" role="form" method="POST" action="{{ Request::url() }}" enctype="multipart/form-data">

		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<input type="hidden" name="url_previous" value="{{ URL::previous() }}">

		<div class="form-group">
			<label class="col-md-3 control-label">Компания</label>
			<div class="col-md-4">
				<select class="form-control" name="company_id">
					@foreach ($companies as $company)
						<option value="{{ $company->id }}" @if ($company->id === $event->company->id) selected @endif >{{ $company->comma_separated_names()['short'] }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Тип</label>
			<div class="col-md-4">
				<select class="form-control" name="event_type_id">
					@foreach ($event_types as $event_type)
						<option value="{{ $event_type->id }}" @if ($event_type->id === $event->event_type_id) selected @endif >{{ $event_type->title }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Заголовок (номер)</label>
			<div class="col-md-4">
				<input class="form-control" name="title" value="{{ $event->title }}">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Количество</label>
			<div class="col-md-4">
				<input class="form-control" name="amount" value="{{ $event->amount }}">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Запланированная дата</label>
			<div class="col-md-4">
				<input class="form-control datepicker" name="scheduled_for" value="@if ($event->scheduled_for){{ $event->scheduled_for->format('d.m.Y H:i') }}@endif">
			</div>
		</div>

		@if ($event->scheduled_for)
			<div class="form-group">
				<label class="col-md-3 control-label">Выполнено</label>
				<div class="col-md-4">
					<select class="form-control" name="achieved">
						<option value="1" @if ($event->achieved === 1) selected @endif >Да</option>
						<option value="null" @if ($event->achieved === null) selected @endif >Нет</option>
					</select>
				</div>
			</div>
		@endif

		<div class="form-group">
			<label class="col-md-3 control-label">Комментарий</label>
			<div class="col-md-4">
				<textarea class="form-control" name="comment">{{ $event->comment }}</textarea>
			</div>
		</div>

		@if ($event->file_name)
			<div class="form-group">
				<label class="col-md-3 control-label">Прикреплённый файл</label>
				<div class="col-md-4">
					<p class="form-control-static"><a href="{{ action('EventController@getAttachment', $event->id) }}">{{ $event->file_name }}</a></p>
				</div>
			</div>
		@endif

		<div class="form-group">
			<label class="col-md-3 control-label">@if ($event->file_name) Заменить @else Загрузить @endif</label>
			<div class="col-md-4">
				<input type="file" class="filestyle" name="file" data-buttonText="">
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</div>
	</form>
</div>
@endsection

@extends('layout')

@section('content')
<div class="panel-heading">{{ $title }}</div>
<div class="panel-body">
	@if (count($events))
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th>п/п</th>
					@if (Auth::user()->access_level->can_access_all_events)
						<th>Менеджер</th>
					@endif
					<th>Время</th>
					<th>Название компании</th>
					<th>Имя</th>
					<th>Телефон</th>
					<th>Цель/комментарий</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($events as $date => $events)
					<tr>
						<td colspan="6"><h4>{{ $date }}</h4></td>
					</tr>
					@foreach ($events as $index => $event)
						<tr @if ($date < date('d.m.Y')) class="danger"@elseif ($date >= date('d.m.Y') && $date < date((date('d')+1).'.m.Y')) class="success"@endif>
							<td>{{ $index+1 }}</td>
							@if (Auth::user()->access_level->can_access_all_events)
								<td>{{ $event->company->manager->name or '' }}</td>
							@endif
							<td>{{ $event->scheduled_for->format('d.m.Y H:i') }}</td>
							<td><a href="{{ action('CompanyController@getConversation', $event->company->id) }}" title="{{ $event->company->comma_separated_names()['full'] }}">{{ $event->company->comma_separated_names()['short'] }}</a></td>
							<td>{{ $event->company->contact_name() }}</td>
							<td>{{ $event->company->contact_phone() }}</td>
							<td>{{ $event->comment }}</td>
						</tr>
					@endforeach
				@endforeach
			</tbody>
		</table>
	</div>
	@else
		<p>Нет запланированных событий.</p>
	@endif
</div>
@endsection

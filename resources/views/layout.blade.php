<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CRM</title>

	<link href="/css/app.css" rel="stylesheet">
	<link href="/css/styles.css" rel="stylesheet">
	<link href="/css/jquery.datetimepicker.css" rel="stylesheet">

	<!-- Fonts -->
	<!-- <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'> -->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	@if (!Auth::guest())
		@include ('partials.nav')
	@endif

	@if (Session::has('message'))
		<div class="container">
			<div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
		</div>
	@endif

	<div class="container">
		<div class="panel panel-default">
			@yield('content')
		</div>
	</div>

	<!-- Scripts -->
	<script src="/js/jquery-2.1.3.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/bootstrap-filestyle.min.js"></script>
	<script src="/js/jquery.datetimepicker.js"></script>
	<script src="/js/jquery.floatThead.js"></script>
	<script src="/js/app.js"></script>
</body>
</html>

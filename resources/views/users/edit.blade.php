@extends('layout')

@section('content')
<div class="panel-heading">Пользователи</div>
<div class="panel-body">
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<form class="form-horizontal" role="form" method="POST" action="{{ action('UserController@update', $user->id) }}">

		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<input type="hidden" name="_method" value="put">

		<div class="form-group">
			<label class="col-md-3 control-label">Имя</label>
			<div class="col-md-4">
				<input class="form-control" name="name" value="{{ $user->name }}">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">E-mail</label>
			<div class="col-md-4">
				<input class="form-control" name="email" value="{{ $user->email }}">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Пароль</label>
			<div class="col-md-4">
				<input class="form-control" name="password" type="password" placeholder="введите новый пароль">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Уровень доступа</label>
			<div class="col-md-4">
				<select class="form-control" name="access_level_id">
					@foreach ($access_levels as $access_level)
						<option value="{{ $access_level->id }}" @if ($access_level->id === $user->access_level_id) selected @endif >{{ $access_level->title }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</div>
	</form>
</div>
@endsection

@extends('layout')

@section('content')
<div class="panel-heading">
	Пользователи
	<a href="{{ action('Auth\AuthController@getRegister') }}" style="float:right;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Добавить</a>
</div>
<div class="panel-body">
	<table class="table">
		<thead>
			<tr>
				<th></th>
				<th>Имя</th>
				<th>Создал</th>
				<th>Обновил</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($users as $index => $user)
				<tr>
					<td>{{ $index+1 }}</td>
					<td>{{ $user->name }}</td>
					<td>@if ($user->created_user) {{ $user->created_user->name }} в {{ $user->created_at->format('d.m.Y H:i') }} @endif</td>
					<td>@if ($user->updated_user) {{ $user->updated_user->name }} в {{ $user->updated_at->format('d.m.Y H:i') }} @endif</td>
					<td>
						<a href="{{ action('UserController@edit', $user->id) }}" title="Редактировать"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
					</td>
					<td>
						<form class="confirm-delete" role="form" method="POST" action="{{ action('UserController@destroy', $user->id) }}">
							<input type="hidden" name="_method" value="delete">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<button class="btn btn-link" type="submit" title="Удалить"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
@include('partials.modal-box')
@endsection

@extends('layout')

@section('content')
<div class="panel-heading">Регистрация</div>
<div class="panel-body">
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<form class="form-horizontal" role="form" method="POST" action="/auth/register">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="form-group">
			<label class="col-md-4 control-label">ФИО</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="name" value="{{ old('name') }}">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-4 control-label">Должность</label>
			<div class="col-md-6">
				<select class="form-control" name="access_level_id">
					@foreach ($access_levels as $access_level)
						<option value="{{ $access_level->id }}" @if ($access_level->id === old('access_level_id')) selected @endif>{{ $access_level->title }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-4 control-label">E-Mail</label>
			<div class="col-md-6">
				<input type="email" class="form-control" name="email" value="{{ old('email') }}">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-4 control-label">Пароль</label>
			<div class="col-md-6">
				<input type="password" class="form-control" name="password">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-4 control-label">Повторите пароль</label>
			<div class="col-md-6">
				<input type="password" class="form-control" name="password_confirmation">
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-6 col-md-offset-4">
				<button type="submit" class="btn btn-primary">
					Регистрация
				</button>
			</div>
		</div>
	</form>
</div>
@endsection

@extends('layout')

@section('content')
<div class="panel-heading">
	Темы заказа
	<a href="{{ action('OrderSubjectController@create') }}" style="float:right;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Добавить</a>
</div>
<div class="panel-body">
	<table class="table">
		<thead>
			<tr>
				<th></th>
				<th>Название</th>
				<th>Создал</th>
				<th>Обновил</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($order_subjects as $index => $subject)
				<tr>
					<td>{{ $index+1 }}</td>
					<td>{{ $subject->title }}</td>
					<td>{{ $subject->created_user->name }} в {{ $subject->created_at->format('d.m.Y H:i') }}</td>
					<td>@if ($subject->updated_user) {{ $subject->updated_user->name }} в {{ $subject->updated_at->format('d.m.Y H:i') }} @endif</td>
					<td>
						<a href="{{ action('OrderSubjectController@edit', $subject->id) }}" title="Редактировать"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
					</td>
					<td>
						<form class="confirm-delete" role="form" method="POST" action="{{ action('OrderSubjectController@destroy', $subject->id) }}">
							<input type="hidden" name="_method" value="delete">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<button class="btn btn-link" type="submit" title="Удалить"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
@include('partials.modal-box')
@endsection

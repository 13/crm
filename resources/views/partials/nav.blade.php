<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!-- <a class="navbar-brand" href="#">Laravel</a> -->
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="/">Главная</a></li>
				<li><a href="{{ action('EventController@getCalls') }}">Звонки <span class="badge">{{ $count['calls'] or '' }}</span></a></li>
				<li><a href="{{ action('EventController@getOffers') }}">КП <span class="badge">{{ $count['offers'] or '' }}</span></a></li>
				<li><a href="{{ action('EventController@getMeetings') }}">Встречи <span class="badge">{{ $count['meetings'] or '' }}</span></a></li>
				@if (Auth::user()->access_level->can_access_all_companies)
					<li><a href="{{ action('CompanyController@getListAll') }}">Вся база <span class="badge">{{ $count['companies-all'] or '' }}</span></a></li>
				@endif
				<li><a href="{{ action('CompanyController@getListIn') }}">Входящие <span class="badge">{{ $count['companies-in'] or '' }}</span></a></li>
				<li><a href="{{ action('CompanyController@getListOut') }}">Исходящие <span class="badge">{{ $count['companies-out'] or '' }}</span></a></li>
				<li><a href="{{ action('CompanyController@getList') }}">Моя база <span class="badge">{{ $count['companies'] or '' }}</span></a></li>
				<li><a href="{{ action('CompanyController@getListForgotten') }}">Забытые компании <span class="badge">{{ $count['companies-forgotten'] or '' }}</span></a></li>
				<li><a href="{{ action('CompanyController@getListArchive') }}">Архив <span class="badge">{{ $count['companies-archive'] or '' }}</span></a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				@if (Auth::user()->access_level->can_edit_regions || Auth::user()->access_level->can_edit_sources || Auth::user()->access_level->can_edit_order_subjects)
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Редактирование <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="{{ action('RegionController@index') }}">Регионы</a></li>
							<li><a href="{{ action('OrderSubjectController@index') }}">Темы заказа</a></li>
							<li><a href="{{ action('SourceController@index') }}">Источники</a></li>
							<li><a href="{{ action('UserController@index') }}">Пользователи</a></li>
						</ul>
					</li>
				@endif
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> {{ Auth::user()->name }} <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/auth/logout">Выйти</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>

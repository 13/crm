@extends('layout')

@section('content')
<div class="panel-heading">
	{{ $title }}
	<a href="{{ action('CompanyController@getCreate') }}" style="float:right; margin-right:20px;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Добавить компанию</a>
</div>
@include('companies.partials.list')
@endsection

@extends('layout')

@section('content')
<div class="panel-heading">Редактирование информации о компании "{{ $company->comma_separated_names()['full'] }}"</div>
<div class="panel-body">
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<form class="form-horizontal" role="form" method="POST" action="{{ Request::url() }}">

		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		@if (Auth::user()->access_level->can_assign_manager)
		<div class="form-group">
			<label class="col-md-3 control-label">Откуда узнали о компании</label>
			<div class="col-md-2">
				<select class="form-control" name="source_type">
					<option value="1" @if ($company->source && '1' === $company->source->is_incoming) selected @endif >Входящий</option>
					<option value="0" @if ($company->source && '0' === $company->source->is_incoming) selected @endif >Исходящий</option>
				</select>
			</div>
			<div class="col-md-2">
				<select class="form-control" name="source_id">
					@foreach ($sources as $source)
						<option value="{{ $source->id }}" data-is-incoming="{{ $source->is_incoming }}" @if ($company->source && $source->id === $company->source->id) selected @endif >{{ $source->title }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<a id="add-source" href="#">Добавить новый источник</a>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Номер входящей заявки</label>
			<div class="col-md-4">
				<input class="form-control" name="request_number" type="number" value="{{ $company->request_number }}">
			</div>
		</div>
		@endif

		@foreach ($company->names as $index => $name)
			<div class="form-group">
				<label class="col-md-3 control-label">Название компании #{{ $index + 1 }}</label>
				<div class="col-md-4">
					<input class="form-control" name="old_company_names[{{ $name->id }}]" value="{{ $name->title }}">
				</div>
			</div>
		@endforeach

		<div class="form-group" id="company-name-group">
			<label class="col-md-3 control-label">Новое название компании</label>
			<div class="col-md-4">
				<input class="form-control" name="company_names[]">
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<a id="add-company-name" href="#">Добавить ещё одно название</a>
			</div>
		</div>

		<hr>

		@foreach ($company->contact_persons as $index => $person)
			<div class="old-contact-person-group">
				<div class="col-md-10 col-md-offset-3">
					<h5 class="contact-person-group-title">Контактное лицо #{{ $index + 1 }}</h5>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">Имя</label>
					<div class="col-md-4">
						<input class="form-control" name="old_contact_persons[{{ $person->id }}][name]" value="{{ $person->name }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">Должность</label>
					<div class="col-md-4">
						<input class="form-control" name="old_contact_persons[{{ $person->id }}][position]" value="{{ $person->position }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">Телефон</label>
					<div class="col-md-4">
						<input class="form-control" name="old_contact_persons[{{ $person->id }}][phone]" type="tel" value="{{ $person->phone }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">E-mail</label>
					<div class="col-md-4">
						<input class="form-control" name="old_contact_persons[{{ $person->id }}][email]" type="email" value="{{ $person->email }}">
					</div>
				</div>
			</div>
		@endforeach

		<div id="contact-person-group">
			<div class="col-md-10 col-md-offset-3">
				<h5 class="contact-person-group-title">Новое контактное лицо</h5>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Имя</label>
				<div class="col-md-4">
					<input class="form-control" name="contact_persons[0][name]">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Должность</label>
				<div class="col-md-4">
					<input class="form-control" name="contact_persons[0][position]">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Телефон</label>
				<div class="col-md-4">
					<input class="form-control" name="contact_persons[0][phone]" type="tel">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">E-mail</label>
				<div class="col-md-4">
					<input class="form-control" name="contact_persons[0][email]" type="email">
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<a id="add-contact-person" href="#">Добавить ещё одно контактное лицо</a>
			</div>
		</div>

		<hr>

		<div class="form-group">
			<label class="col-md-3 control-label">Регион</label>
			<div class="col-md-4">
				<select class="form-control" name="region_id">
					<option value="0">(Не выбрано)</option>
					@foreach ($regions as $region)
						<option value="{{ $region->id }}" @if ($company->region && $region->id === $company->region->id) selected @endif >{{ $region->title }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<a id="add-region" href="#">Добавить новый регион</a>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Адрес</label>
			<div class="col-md-4">
				<textarea class="form-control" name="address">{{ $company->address }}</textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Website</label>
			<div class="col-md-4">
				<input class="form-control" name="website" value="{{ $company->website }}">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Описание</label>
			<div class="col-md-4">
				<textarea class="form-control" name="description">{{ $company->description }}</textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Реквизиты</label>
			<div class="col-md-4">
				<textarea class="form-control" name="requisites">{{ $company->requisites }}</textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Тематика заказа</label>
			<div class="col-md-4">
				<select class="form-control" name="order_subject_id">
					<option value="0">(Не выбрано)</option>
					@foreach ($order_subjects as $order_subject)
						<option value="{{ $order_subject->id }}" @if ($company->order_subject && $order_subject->id === $company->order_subject->id) selected @endif >{{ $order_subject->title }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<a id="add-order-subject" href="#">Добавить новую тематику</a>
			</div>
		</div>

		@if (Auth::user()->access_level->can_assign_manager)
			<div class="form-group">
				<label class="col-md-3 control-label">Менеджер</label>
				<div class="col-md-4">
					<select class="form-control" name="current_manager_id">
						@foreach ($managers as $manager)
							<option value="{{ $manager->id }}" @if ($manager->id === $company->manager->id) selected @endif >{{ $manager->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		@endif

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</div>
	</form>
</div>
@endsection

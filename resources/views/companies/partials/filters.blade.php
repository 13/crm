<div class="collapse @if (Request::all()) in @endif" id="collapse-filters">
	<div class="well" style="margin:20px;">
		<form class="form-horizontal" role="form" method="GET" action="{{ Request::url() }}">

			<div class="form-group">
				<label class="col-md-3 control-label">Период</label>
				<div class="col-md-2">
					<input class="form-control datepicker" name="period[from]" placeholder="с" value="{{ Request::input('period.from') }}">
				</div>
				<div class="col-md-2">
					<input class="form-control datepicker" name="period[to]" placeholder="по" value="{{ Request::input('period.to') }}">
				</div>
			</div>

			@if (Auth::user()->access_level->can_assign_manager)
				<div class="form-group">
					<label class="col-md-3 control-label">Менеджер</label>
					<div class="col-md-4">
						<select multiple class="form-control" name="managers[]">
							@foreach ($managers as $manager)
								<option value="{{ $manager->id }}" @if (in_array($manager->id, (array)Request::input('managers'))) selected @endif>{{ $manager->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			@endif

			<div class="form-group">
				<label class="col-md-3 control-label">Регион</label>
				<div class="col-md-4">
					<select multiple class="form-control" name="regions[]">
						@foreach ($regions as $region)
							<option value="{{ $region->id }}" @if (in_array($region->id, (array)Request::input('regions'))) selected @endif>{{ $region->title }}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Действие</label>
				<div class="col-md-2">
					<select class="form-control" name="event_status">
						<option value="">Все</option>
						<option value="1" @if (1 === (int) Request::input('event_status')) selected @endif>Назначено</option>
						<option value="2" @if (2 === (int) Request::input('event_status')) selected @endif>Состоялось</option>
					</select>
				</div>
				<div class="col-md-2">
					<select class="form-control" name="event_type">
						<option value="">Все</option>
						@foreach ($event_types as $event_type)
							<option value="{{ $event_type->id }}" @if ($event_type->id === Request::input('event_type')) selected @endif>{{ $event_type->title }}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">На сумму</label>
				<div class="col-md-2">
					<input type="number" class="form-control" name="amount[from]" placeholder="от" value="{{ Request::input('amount.from') }}">
				</div>
				<div class="col-md-2">
					<input type="number" class="form-control" name="amount[to]" placeholder="до" value="{{ Request::input('amount.to') }}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Дата последней активности</label>
				<div class="col-md-4">
					<input class="form-control datepicker" name="last_event_at" value="{{ Request::input('last_event_at') }}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Дата начала работы с компанией</label>
				<div class="col-md-2">
					<input class="form-control datepicker" name="confirmed[from]" placeholder="с" value="{{ Request::input('confirmed.from') }}">
				</div>
				<div class="col-md-2">
					<input class="form-control datepicker" name="confirmed[to]" placeholder="по" value="{{ Request::input('confirmed.to') }}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Откуда заявки</label>
				<div class="col-md-2">
					<select class="form-control" name="source_type">
						<option value="">Все</option>
						<option value="1" @if ('1' === (string) Request::input('source_type')) selected @endif>Входящий</option>
						<option value="0" @if ('0' === (string) Request::input('source_type')) selected @endif>Исходящий</option>
					</select>
				</div>
				<div class="col-md-2">
					<select class="form-control" name="source_id">
						<option value="">Все</option>
						@foreach ($sources as $source)
							<option value="{{ $source->id }}" data-is-incoming="{{ $source->is_incoming }}" @if ($source->id === Request::input('source_id')) selected @endif>{{ $source->title }}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Тематика заявки</label>
				<div class="col-md-4">
					<select class="form-control" name="order_subject_id">
						<option value="">Все</option>
						@foreach ($order_subjects as $order_subject)
							<option value="{{ $order_subject->id }}" @if ($order_subject->id === Request::input('order_subject_id')) selected @endif>{{ $order_subject->title }}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-10 col-md-offset-3">
					<button type="submit" class="btn btn-primary">Сформировать</button>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="panel-body">
	@if (count($companies))
	<div class="table-responsive">
		<table class="table with-floating-header">
			<thead>
				<tr>
					<th>Исх / Вход</th>
					@if (Auth::user()->access_level->can_access_all_companies)
						<th>Менеджер</th>
					@endif
					<th>КП</th>
					<th>Тоннаж</th>
					<th>Назначено действие</th>
					<th>Название компании</th>
					<th>Контактное лицо</th>
					<th>Номер телефона</th>
					<th>Эл. почта</th>
					<th>Переговоры</th>
					<th>Статус</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($companies as $company)
					<tr class="color-{{ $company->condition->id or 'default' }}@if ((int) Session::get('added_company_id') === (int) $company->id) success @endif">
						<td>@if ($company->request_number !== null) вход {{ $company->request_number }} @else исх @endif</td>
						@if (Auth::user()->access_level->can_access_all_companies)
							<td>{{ $company->manager->name or '' }}</td>
						@endif
						<td>{{ $company->last_offer_title() }}</td>
						<td>{{ $company->last_offer_amount() }}</td>
						<td>@if (count($company->events_scheduled)) {{ $company->events_scheduled->last()->scheduled_for->format('d.m.y H:i') }} {{ $company->events_scheduled->last()->type->title }} @endif</td>
						<td><a href="{{ action('CompanyController@getView', $company->id) }}" title="{{ $company->comma_separated_names['full'] }}">{{ $company->comma_separated_names['short'] }}</a></td>
						<td>
							@foreach ($company->contact_persons as $contact_person)
								{{ $contact_person->name }}<br>
							@endforeach
						</td>
						<td>
							@foreach ($company->contact_persons as $contact_person)
								{{ $contact_person->phone }}<br>
							@endforeach
						</td>
						<td>
							@foreach ($company->contact_persons as $contact_person)
								{{ $contact_person->email }}<br>
							@endforeach
						</td>
						<td>
							@foreach ($company->last_comments(3) as $comment)
								{{ $comment['date']->format('d.m.Y') }} &mdash; {{ $comment['text'] }}<br>
							@endforeach
						</td>
						<td>{{ $company->condition->title or '' }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@else
		<p>Записи отсутствуют.</p>
	@endif
</div>

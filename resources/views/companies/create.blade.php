@extends('layout')

@section('content')
<div class="panel-heading">Заявка на внесение новой компании в базу</div>
<div class="panel-body">
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<form id="create-company" class="form-horizontal" role="form" method="POST" action="{{ Request::url() }}">

		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="form-group">
			<label class="col-md-3 control-label">Откуда узнали о компании</label>
			<div class="col-md-2">
				<select class="form-control" name="source_type">
					<option value="1">Входящий</option>
					<option value="0">Исходящий</option>
				</select>
			</div>
			<div class="col-md-2">
				<select class="form-control" name="source_id">
					@foreach ($sources as $source)
						<option value="{{ $source->id }}" data-is-incoming="{{ $source->is_incoming }}">{{ $source->title }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<a id="add-source" href="#">Добавить новый источник</a>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Номер входящей заявки</label>
			<div class="col-md-4">
				<input class="form-control" name="request_number" type="number">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Первоначальный запрос</label>
			<div class="col-md-4">
				<textarea class="form-control" rows="12" name="request"></textarea>
			</div>
		</div>

		<div class="form-group" id="company-name-group">
			<label class="col-md-3 control-label">Название компании</label>
			<div class="col-md-4">
				<input class="form-control" name="company_names[]">
			</div>
			@if ($errors->has('company_title')) <p class="help-block">{{ $errors->first('company_title') }}</p> @endif
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<a id="add-company-name" href="#">Добавить ещё одно название</a>
			</div>
		</div>

		<hr>

		<div id="contact-person-group">
			<div class="col-md-10 col-md-offset-3">
				<h5 class="contact-person-group-title">Контактное лицо</h5>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Имя</label>
				<div class="col-md-4">
					<input class="form-control" name="contact_persons[0][name]">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Должность</label>
				<div class="col-md-4">
					<input class="form-control" name="contact_persons[0][position]">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Телефон</label>
				<div class="col-md-4">
					<input class="form-control" name="contact_persons[0][phone]" type="tel">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">E-mail</label>
				<div class="col-md-4">
					<input class="form-control" name="contact_persons[0][email]" type="email">
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<a id="add-contact-person" href="#">Добавить ещё одно контактное лицо</a>
			</div>
		</div>

		<hr>

		<div class="form-group">
			<label class="col-md-3 control-label">Регион</label>
			<div class="col-md-4">
				<select class="form-control" name="region_id">
					<option value="0">(Не выбрано)</option>
					@foreach ($regions as $region)
						<option value="{{ $region->id }}">{{ $region->title }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<a id="add-region" href="#">Добавить новый регион</a>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Адрес</label>
			<div class="col-md-4">
				<textarea class="form-control" name="address"></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Website</label>
			<div class="col-md-4">
				<input class="form-control" name="website">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Описание</label>
			<div class="col-md-4">
				<textarea class="form-control" name="description"></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Реквизиты</label>
			<div class="col-md-4">
				<textarea class="form-control" name="requisites"></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Тематика заказа</label>
			<div class="col-md-4">
				<select class="form-control" name="order_subject_id">
					<option value="0">(Не выбрано)</option>
					@foreach ($order_subjects as $order_subject)
						<option value="{{ $order_subject->id }}">{{ $order_subject->title }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<a id="add-order-subject" href="#">Добавить новую тематику</a>
			</div>
		</div>

		@if (Auth::user()->access_level->can_assign_manager)
			<div class="form-group">
				<label class="col-md-3 control-label">Менеджер</label>
				<div class="col-md-4">
					<select class="form-control" name="current_manager_id">
						@foreach ($managers as $manager)
							<option value="{{ $manager->id }}" @if ($manager->id === Auth::user()->id) selected @endif >{{ $manager->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		@endif

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<button type="submit" class="btn btn-primary">Добавить</button>
			</div>
		</div>
	</form>
</div>
<div id="notice-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">В базе данных обнаружены дубликаты</h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-default">Ок</button>
			</div>
		</div>
	</div>
</div>
@endsection

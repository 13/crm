@extends('layout')

@section('content')
<div class="panel-heading">
	{{ $company->comma_separated_names()['full'] }}
	<a href="{{ action('CompanyController@getEdit', $company->id) }}" style="float:right;"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Редактировать</a>
</div>
<div class="panel-body">
	@include('companies.nav', ['current' => 'info'])
	<table class="table" style="margin-top:20px;">
		<tbody>
			<tr>
				<td style="border: none;">Менеджер</td>
				<td style="border: none;">{{ $company->manager->name or '' }}</td>
			</tr>
			<tr>
				<td>Название компании</td>
				<td>
					@foreach ($company->names as $name)
						{{ $name->title }}<br>
					@endforeach
				</td>
			</tr>
			<tr>
				<td>Контактное лицо</td>
				<td>
					@foreach ($company->contact_persons as $contact_person)
						<p>
							{{ $contact_person->name }}
							@if ('' !== $contact_person->position)
								({{ $contact_person->position }})
							@endif
							@if ('' !== $contact_person->phone)
								<br>{{ $contact_person->phone }}
							@endif
							@if ('' !== $contact_person->email)
								<br>{{ $contact_person->email }}
							@endif
						</p>
					@endforeach
				</td>
			</tr>
			<tr>
				<td>Регион</td>
				<td>{{ $company->region->title or '' }}</td>
			</tr>
			<tr>
				<td>Адрес</td>
				<td>{{ $company->address }}</td>
			</tr>
			<tr>
				<td>E-mail</td>
				<td>{{ $company->email }}</td>
			</tr>
			<tr>
				<td>Сайт</td>
				<td>
					@if (!empty($company->website))
						<a href="@if (substr($company->website, 0, 4) !== 'http')http://@endif{{ $company->website }}" target="_blank">{{ $company->website }}</a>
					@endif
				</td>
			</tr>
			<tr>
				<td>Дата начала работы</td>
				<td>{{ $company->confirmed_at->format('d.m.Y H:i') }}</td>
			</tr>
			<tr>
				<td>Дата последней активности</td>
				<td>@if ($company->last_event_at) {{ $company->last_event_at->format('d.m.Y H:i') }} @endif</td>
			</tr>
			<tr>
				<td>Описание</td>
				<td>
					{!! nl2br(e($company->description)) !!}
				</td>
			</tr>
			<tr>
				<td>Реквизиты</td>
				<td>
					{!! nl2br(e($company->requisites)) !!}
				</td>
			</tr>
		</tbody>
	</table>
</div>
@endsection

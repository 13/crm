<?php $index = 0; ?>
@extends('layout')

@section('content')
<div class="panel-heading">{{ $company->comma_separated_names()['full'] }}</div>
<div class="panel-body">
	@include('companies.nav', ['current' => 'attachments'])

	@if (count($company->events))
		<div class="table-responsive" style="margin-top:20px;">
			<table class="table table-striped">
				@foreach ($company->events as $event)
					<tr>
						<td>{{ ++$index }}</td>
						<td>{{ $event->created_at->format('d.m.y H:i') }}</td>
						<td>{{ $event->manager->name or '' }}</td>
						<td>
							<span class="label label-default">{{ $event->type->title or '' }}</span>
							<span class="label label-success">{{ $event->title }}</span>
						</td>
						<td>
							@if ($event->file_name)
								<a href="{{ action('EventController@getAttachment', $event->id) }}">
									<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
								</a>
							@endif
						</td>
						@if (Auth::user()->access_level->can_edit_events)
							<td><a href="{{ action('EventController@getEdit', $event->id) }}" title="Редактировать"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
							<td>
								<form class="confirm-delete" role="form" method="POST" action="{{ action('EventController@deleteRemove', $event->id) }}">
									<input type="hidden" name="_method" value="delete">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<button class="btn btn-link" type="submit" title="Удалить"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
								</form>
							</td>
						@endif
					</tr>
				@endforeach
			</table>
		</div>
	@else
		<p style="margin-top:20px;">Записи отсутствуют.</p>
	@endif

	<form id="attachments-form" class="form-inline" role="form" method="POST" action="{{ action('CompanyController@postConversation', $company->id) }}" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="event_type_id" value="6">
		<input type="file" class="filestyle" name="file" data-buttonText="Файл" data-buttonBefore="true">
		<input class="form-control" type="text" placeholder="Имя" name="title" disabled="disabled">
		<button type="submit" class="btn btn-primary" disabled="disabled">Добавить!</button>
	</form>
</div>
@include('partials.modal-box')
@endsection

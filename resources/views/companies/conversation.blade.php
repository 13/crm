@extends('layout')

@section('content')
<div class="panel-heading">{{ $company->comma_separated_names()['full'] }}</div>
<div class="panel-body">
	@include('companies.nav', ['current' => 'conversation'])
	@if (count($company->events))
		<div id="conversation-list" class="panel panel-default" style="margin-top: 20px;">
			<div class="table-responsive">
				<table class="table table-hover">
					@foreach ($company->events as $event)
						<tr>
							<td class="text-muted">{{ $event->created_at->format('d.m.y H:i') }}</td>
							<td>@if ($event->scheduled_for){{ $event->scheduled_for->format('d.m.y H:i') }}&nbsp;<span class="glyphicon glyphicon-@if ('achieved' === $event->status())ok-circle text-success @elseif ('scheduled' === $event->status())time text-warning @endif" aria-hidden="true"></span>@endif</td>
							<td>
								<span class="label label-default">{{ $event->type->title }}</span>
								<span class="label label-success">{{ $event->title }}</span>
							</td>
							<td>{!! nl2br(e($event->comment)) !!}</td>
							@if (Auth::user()->access_level->can_edit_events)
								<td><a href="{{ action('EventController@getEdit', $event->id) }}" title="Редактировать"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
								<td>
									<form class="confirm-delete" role="form" method="POST" action="{{ action('EventController@deleteRemove', $event->id) }}">
										<input type="hidden" name="_method" value="delete">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<button class="btn btn-link" type="submit" title="Удалить"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
									</form>
								</td>
							@endif
						</tr>
					@endforeach
				</table>
			</div>
		</div>
	@else
		<p style="margin-top:20px;">Записи отсутствуют.</p>
	@endif

	<form id="conversation-form" role="form" method="POST" action="{{ Request::url() }}" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="row">
			<div class="col-md-2 input-block">
				<div class="input-group input-row">
					<span class="input-group-btn">
						<label class="btn btn-primary"><input type="radio" name="event_type_id" class="scheduled" value="1"> Зв</label>
					</span>
					<input class="form-control datepicker" name="scheduled_for" placeholder="дата" disabled="disabled">
				</div>
				<label class="btn btn-default btn-block input-row"><input type="radio" name="event_type_id" class="happened" value="1"> Созвонились</label>
			</div>
			<div class="col-md-2 input-block">
				<div class="input-group input-row">
					<span class="input-group-btn">
						<label class="btn btn-primary"><input type="radio" name="event_type_id" class="scheduled" value="3"> КП</label>
					</span>
					<input class="form-control datepicker" name="scheduled_for" placeholder="дата" disabled="disabled">
				</div>
				<div class="input-row">
					<input type="file" class="filestyle" name="file" data-buttonText="КП" data-buttonBefore="true">
				</div>
				<input class="form-control input-row" type="text" placeholder="Номер" name="title" disabled="disabled">
				<input class="form-control input-row" type="text" placeholder="Сумма (тоннаж)" name="amount" disabled="disabled">
			</div>
			<div class="col-md-2 input-block">
				<div class="input-group input-row">
					<span class="input-group-btn">
						<label class="btn btn-primary"><input type="radio" name="event_type_id" class="scheduled" value="2"> Встр</label>
					</span>
					<input class="form-control datepicker" name="scheduled_for" placeholder="дата" disabled="disabled">
				</div>
				<label class="btn btn-default btn-block input-row"><input type="radio" name="event_type_id" class="happened" value="2"> Встретились</label>
			</div>
			<div class="col-md-2 input-block">
				<div class="input-group input-row">
					<span class="input-group-btn">
						<label class="btn btn-primary"><input type="radio" name="event_type_id" class="scheduled" value="4"> Догов</label>
					</span>
					<input class="form-control datepicker" name="scheduled_for" placeholder="дата" disabled="disabled">
				</div>
				<div class="input-row">
					<input type="file" class="filestyle" name="file" data-buttonText="Дог" data-buttonBefore="true">
				</div>
				<input class="form-control input-row" type="text" placeholder="Сумма" name="amount" disabled="disabled">
			</div>
			<div class="col-md-2 input-block">
				<div class="input-group input-row">
					<span class="input-group-btn">
						<label class="btn btn-primary"><input type="radio" name="event_type_id" class="scheduled" value="5"> Оплата</label>
					</span>
					<input class="form-control datepicker" name="scheduled_for" placeholder="дата" disabled="disabled">
				</div>
				<label class="btn btn-default btn-block input-row"><input type="radio" name="event_type_id" class="happened" value="5"> Оплачено</label>
				<input class="form-control input-row" type="text" placeholder="Сумма" name="amount" disabled="disabled">
			</div>
			<div class="col-md-2 input-block">
				<select class="form-control input-row" name="condition_id" form="update-status-form">
					<option value="0">(Не выбрано)</option>
					@foreach ($conditions as $condition)
						<option value="{{ $condition->id }}" @if ($company->condition && $condition->id === $company->condition->id) selected @endif >{{ $condition->title }}</option>
					@endforeach
				</select>
				<button type="submit" class="btn btn-default btn-block" form="update-status-form">Обновить</button>
			</div>
		</div>

		<div class="comment-block">
			<div class="form-group">
				<label>Комментарий</label>
				<textarea class="form-control" name="comment" disabled="disabled"></textarea>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary" disabled="disabled">Добавить!</button>
			</div>
		</div>
	</form>
	<form id="update-status-form" role="form" method="POST" action="{{ action('CompanyController@postUpdateStatus') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="company_id" value="{{ $company->id }}">
	</form>
</div>
@include('partials.modal-box')
@endsection

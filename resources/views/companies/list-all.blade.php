@extends('layout')

@section('content')
<div class="panel-heading">
	@if (Auth::user()->access_level->can_access_all_companies)Вся@elseМоя@endif база
	<a data-toggle="collapse" href="#collapse-filters" aria-expanded="false" aria-controls="collapse-filters" style="float:right;"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Фильтры</a>
	<a href="{{ action('CompanyController@getCreate') }}" style="float:right; margin-right:20px;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Добавить компанию</a>
</div>
@include('companies.partials.filters')
@include('companies.partials.list')
@endsection

<ul class="nav nav-tabs">
	<li role="presentation"@if ('info' == $current) class="active"@endif>
		<a href="{{ action('CompanyController@getView', $company->id) }}"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Основные данные</a>
	</li>
	<li role="presentation"@if ('conversation' == $current) class="active"@endif>
		<a href="{{ action('CompanyController@getConversation', $company->id) }}"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Переговоры</a>
	</li>
	<li role="presentation"@if ('attachments' == $current) class="active"@endif>
		<a href="{{ action('CompanyController@getAttachments', $company->id) }}"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span> Вложения</a>
	</li>
</ul>

@extends('layout')

@section('content')
<div class="panel-heading">Источники</div>
<div class="panel-body">
	<form class="form-horizontal" role="form" method="POST" action="{{ action('SourceController@update', $source->id) }}">

		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<input type="hidden" name="_method" value="put">

		<div class="form-group">
			<label class="col-md-3 control-label">Название</label>
			<div class="col-md-4">
				<input class="form-control" name="title" value="{{ $source->title }}">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Тип</label>
			<div class="col-md-4">
				<select class="form-control" name="is_incoming">
					<option value="1" @if ('1' === $source->is_incoming) selected @endif >Входящий</option>
					<option value="0" @if ('0' === $source->is_incoming) selected @endif >Исходящий</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</div>
	</form>
</div>
@endsection

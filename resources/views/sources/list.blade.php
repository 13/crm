@extends('layout')

@section('content')
<div class="panel-heading">
	Источники
	<a href="{{ action('SourceController@create') }}" style="float:right;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Добавить</a>
</div>
<div class="panel-body">
	<table class="table">
		<thead>
			<tr>
				<th></th>
				<th>Название</th>
				<th>Создал</th>
				<th>Обновил</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($sources_grouped as $is_incoming => $sources)
				<tr>
					<td colspan="6"><h4>{!! $is_incoming === 1? 'Входящие' : 'Исходящие' !!}</h4></td>
				</tr>
				@foreach ($sources as $index => $source)
					<tr>
						<td>{{ $index+1 }}</td>
						<td>{{ $source->title }}</td>
						<td>{{ $source->created_user->name }} в {{ $source->created_at->format('d.m.Y H:i') }}</td>
						<td>@if ($source->updated_user) {{ $source->updated_user->name }} в {{ $source->updated_at->format('d.m.Y H:i') }} @endif</td>
						<td>
							<a href="{{ action('SourceController@edit', $source->id) }}" title="Редактировать"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
						</td>
						<td>
							<form class="confirm-delete" role="form" method="POST" action="{{ action('SourceController@destroy', $source->id) }}">
								<input type="hidden" name="_method" value="delete">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<button class="btn btn-link" type="submit" title="Удалить"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
							</form>
						</td>
					</tr>
				@endforeach
			@endforeach
		</tbody>
	</table>
</div>
@include('partials.modal-box')
@endsection

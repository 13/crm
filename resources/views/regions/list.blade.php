@extends('layout')

@section('content')
<div class="panel-heading">
	Регионы
	<a href="{{ action('RegionController@create') }}" style="float:right;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Добавить</a>
</div>
<div class="panel-body">
	<table class="table">
		<thead>
			<tr>
				<th></th>
				<th>Название региона</th>
				<th>Создал</th>
				<th>Обновил</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($regions as $index => $region)
				<tr>
					<td>{{ $index+1 }}</td>
					<td>{{ $region->title }}</td>
					<td>@if ($region->created_user) {{ $region->created_user->name }} в {{ $region->created_at->format('d.m.Y H:i') }} @endif</td>
					<td>@if ($region->updated_user) {{ $region->updated_user->name }} в {{ $region->updated_at->format('d.m.Y H:i') }} @endif</td>
					<td>
						<a href="{{ action('RegionController@edit', $region->id) }}" title="Редактировать"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
					</td>
					<td>
						<form class="confirm-delete" role="form" method="POST" action="{{ action('RegionController@destroy', $region->id) }}">
							<input type="hidden" name="_method" value="delete">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<button class="btn btn-link" type="submit" title="Удалить"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
@include('partials.modal-box')
@endsection

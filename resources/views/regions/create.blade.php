@extends('layout')

@section('content')
<div class="panel-heading">Регионы</div>
<div class="panel-body">
	<form class="form-horizontal" role="form" method="POST" action="{{ action('RegionController@store') }}">

		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="form-group">
			<label class="col-md-3 control-label">Название</label>
			<div class="col-md-4">
				<input class="form-control" name="title">
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-10 col-md-offset-3">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</div>
	</form>
</div>
@endsection

@extends('layout')

@section('content')
<div class="panel-heading">Главная</div>
<div class="panel-body">
	@if (count($events))
		<table class="table">
			<thead>
				<tr>
					@if (Auth::user()->access_level->can_access_all_events)
						<th>Менеджер</th>
					@endif
					<th>Дата</th>
					<th>Действие</th>
					<th>Тонн</th>
					<th>Название компании</th>
					<th>Последний комментарий</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($events as $title => $events)
					<tr>
						<td colspan="6"><h4>{{ $title }}</h4></td>
					</tr>
					@foreach ($events as $index => $event)
						<tr @if ($event->scheduled_for < Carbon\Carbon::today()) class="danger"@elseif ($event->scheduled_for >= Carbon\Carbon::today() && $event->scheduled_for < Carbon\Carbon::tomorrow()) class="success"@endif>
							@if (Auth::user()->access_level->can_access_all_events)
								<td>{{ $event->company->manager->name or '' }}</td>
							@endif
							<td>{{ $event->scheduled_for->format('d.m.y H:i') }}</td>
							<td>{{ $event->type->title }}</td>
							<td>
								@if (!is_null($event->company->last_offer_amount()))
									{{ $event->company->last_offer_amount() }} т
								@endif
							</td>
							<td><a data-title="{{ $event->company->contact_name() }}" data-content="<p>{{ $event->company->contact_phone() }}</p><p>{{ $event->company->contact_email() }}</p><p>{{ $event->company->website or '' }}</p>" class="company-name" href="{{ action('CompanyController@getView', $event->company->id) }}">{{ $event->company->comma_separated_names()['short'] }}</a></td>
							<td>{{ $event->company->events->last()->comment }}</td>
						</tr>
					@endforeach
				@endforeach
			</tbody>
		</table>
	@else
		<p>Нет запланированных событий.</p>
	@endif
</div>
@endsection
